import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import argparse

import random
import torch
from augmentation.trainer import Trainer
from augmentation.trainer import Loss_Entropy_Loader
from augmentation.Augmenter import Augmenter
from data_utils.SeqDataGenerator import SeqDataCollector
from data_utils.RankingEvaluator import print_dict
from baseline.FPMC import FPMC
from baseline.Caser import Caser
from baseline.GRU4Rec import GRU4Rec
from baseline.SeqRec import SeqRec
from baseline.SASRec import SASRec
import time
from joint_main import context_select

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default='cd')
parser.add_argument('--rec', default='fpmc')
args = parser.parse_args()
torch.multiprocessing.set_sharing_strategy('file_system')
# if torch.cuda.is_available():
#     torch.set_default_tensor_type('torch.cuda.FloatTensor')

if __name__ == '__main__':
    config = {
        # data settings
        'save_epochs': [1, 5, 10, 20, 50, 100, 200, 300],
        'filter_test': True,
        'dataset': 'ml2k',
        'direction': 'uni',  # bi / uni / comp
        'aug_num': 3,
        'eval_neg_num': 100,
        'max_seq_len': 200,
        'max_pred_num': 10,
        'mask_prob': 0.15,
        'dupe_num': 10,
        'train_neg_num': 10,
        'input_len': 5,
        'threshold': 0.1,  # 0.05 for steam
        'cand_num': 500,
        'noise_ratio': 0.1,
        'noise_type': 'raw',  # new, raw, no
        'item_item_window': 40,
        'thread_num': 4,
        'analysis_sample_size': 500,
        'analysis_iter_length': 200,
        # training settings
        'rec_model': 'caser',
        'train_type': 'analysis',  # train / analysis / eval
        'save_loss_entropy': False,
        'epoch_num': 500,
        'learning_rate': 1e-2,
        'train_batch_size': 8000,
        'test_batch_size': 256,
        'drop_ratio': 0.1,
        # sample selection
        'paced_loss': False,
        'paced_loss_slope': False,
        'paced_entropy': False,
        'paced_entropy_slope': False,
        'forget_rate': 0.06,
        'slope_alpha': 0.05,
        'num_gradual': 20,  # 20
        # graph settings
        'n_layers': 1,
        'next_hop_num': 1,
        'graph_dropout': False,
        # prob settings
        'entropy_threshold': 1.0,
        'sample_num': 4,
        'sample_loss_weight': 0.01,
        'entropy_num': 4,
        'entropy_reg_weight': 1,
        # SAS settings
        'decay_factor': 0.9,
        'hidden_size': 32,
        'num_hidden_layers': 1,
        'num_attention_heads': 2,
        'intermediate_size': 64,
        'hidden_act': "gelu",
        'hidden_dropout_prob': 0.1,
        'attention_probs_dropout_prob': 0.1,
        'type_vocab_size': 1,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'use_item_bias': True,
        # 'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        # Caser Config
        'n_h': 16,
        'n_v': 4,
        # GRU4Rec config
        'gru_layer_num': 2,

    }

random.seed(123)


def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    config['dataset'] = args.dataset
    config['rec_model'] = args.rec
    for dataset in ['ml2k']: # 'ml2k', 'steam', 'cd', 'electronics'
        config['dataset'] = dataset
        for rec_model in ['caser']:
            config['rec_model'] = rec_model
            for noise_type in ['raw']:
                for iter_num in [100]:
                    config['analysis_iter_length'] = iter_num
                    config['noise_type'] = noise_type
                    for sample_size in [1000]:
                        config['analysis_sample_size'] = sample_size
                        print_dict(config, 'config')
                        data_model = SeqDataCollector(config)
                        # model_rec = SeqRec(config, context_encoder=context_select(config))
                        # trainer = Trainer(config, None, None, data_model, loss_entropy,
                        #                   save_dir='./datasets/' + config['dataset'] + '/seq/')
                        # trainer.run_self()




if __name__ == '__main__':
    main()
