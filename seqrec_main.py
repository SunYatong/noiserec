from augmentation.BERT_SeqRec import BertForSeqRec
from augmentation.BERT_SeqRec import BertConfig
from augmentation.BERT_SeqRec import fit_batch
from data_utils.SeqDataGenerator import SeqDataCollector
from data_utils.RankingEvaluator import RankingEvaluator
from data_utils.RankingEvaluator import print_dict
import torch


def train_model(config):
    print_dict(config, 'config')
    data_collector = SeqDataCollector(config)
    recommender = BertForSeqRec(BertConfig(data_collector.numItem, config))
    optimizer = torch.optim.Adam(recommender.parameters(), lr=config['learning_rate'])
    test_evaluator = RankingEvaluator(data_collector.get_test_batches(50))

    print('#'*10 + ' Start Training ' + '#'*10)
    keep_train = True
    for epoch in range(config['epoch_num']):
        train_batches = data_collector.get_train_batches(config['batch_size'])
        iter_loss = 0
        for i, train_batch in enumerate(train_batches):
            batch_loss = fit_batch(train_batch,  recommender, optimizer)
            iter_loss += batch_loss
        print(f"loss: {iter_loss / len(train_batches)}")
        if epoch % 20 == 0:
            if keep_train:
                keep_train = test_evaluator.evaluate(model=recommender, train_iter=epoch)
            else:
                print('early stop')


if __name__ == '__main__':
    config = {
        'data_path': './datasets/ml1m/seq/',
        'eval_neg_num': 100,
        'max_seq_len': 200,
        'max_pred_num': 10,
        'mask_prob': 0.1,
        'dupe_num': 10,
        'neg_num': 5,
        # training settings
        'epoch_num': 3000,
        'rec_learning_rate': 1e-4,
        'aug_learning_rate': 1e-4,
        'train_batch_size': 256,
        'valid_batch_size': 256,
        'aug_pretrain_epochs': 300,
        'rec_pretrain_epochs': 300,
        # model settings
        'hidden_size': 128,
        'initializer_range': 0.02,
        'use_item_bias': True,
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    }

    for loss_type in ['pairwise_sample', 'pointwise_sample']:
        for neg_num in [3, 5, 7, 10]:
            config['loss_type'] = loss_type
            config['neg_num'] = neg_num
            train_model(config)
