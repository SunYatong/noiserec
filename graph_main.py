import os

os.environ["CUDA_VISIBLE_DEVICES"] = "2"
import argparse

import random
import torch
from augmentation.trainer import Trainer
from data_utils.GraphDataGenerator import GraphDataCollector
from data_utils.RankingEvaluator import print_dict
from baseline.FPMC import ProbFPMC
from baseline.GraphSeq import RobGraphSeqRec
from baseline.GRU4Rec import RobGRU4Rec
from baseline.RobustSeqRec import RobustSeqRec
from baseline.SASRec import RobGraphSASRec
from baseline.SASRec import RecentRobGraphSASRec
from baseline.SASRec import LightRobGraphSASRec
from baseline.RobustCaser import RobustCaser

import time

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default='cd')
parser.add_argument('--rec', default='graph_rob_sasrec')
parser.add_argument('--lr', default=0.01)
parser.add_argument('--l2', default=0.01)
parser.add_argument('--sample', default=0.01)
args = parser.parse_args()
torch.multiprocessing.set_sharing_strategy('file_system')
# if torch.cuda.is_available():
#     torch.set_default_tensor_type('torch.cuda.FloatTensor')

def context_select(config):
    if config['rec_model'] == 'rob_gru4rec':
        return RobGRU4Rec(config)
    elif config['rec_model'] == 'rob_caser':
        return RobustCaser(config)
    elif config['rec_model'] == 'fpmc':
        return ProbFPMC(config)
    elif config['rec_model'] == 'graph_rob_sasrec':
        return RobGraphSASRec(config)
    elif config['rec_model'] == 'recent_sasrec':
        return RecentRobGraphSASRec(config)
    elif config['rec_model'] == 'light_sasrec':
        return LightRobGraphSASRec(config)
    else:
        print(config['rec_model'] + ' does not match any model, run RobGRU4Rec')
        return RobGRU4Rec(config)


if __name__ == '__main__':
    config = {
        # data settings
        'save_epochs': [1, 5, 10, 20, 50, 100, 200, 300],
        'filter_test': False,
        'dataset': 'ml2k',
        'direction': 'uni', # bi / uni / comp
        'aug_num': 3,
        'eval_neg_num': 100,
        'max_seq_len': 200,
        'max_pred_num': 10,
        'mask_prob': 0.15,
        'dupe_num': 10,
        'train_neg_num': 10,
        'input_len': 5,
        'threshold': 0.1,  # 0.05 for steam
        'cand_num': 500,
        'noise_ratio': 0.1,
        'noise_type': 'raw',  # new, raw, no
        'item_item_window': 40,
        'thread_num': 4,
        'analysis_sample_size': 500,
        'analysis_iter_length': 200,
        # training settings
        'rec_model': 'caser',
        'train_type': 'train',  # train / analysis / eval
        'save_loss_entropy': False,
        'epoch_num': 500,
        'learning_rate': 1e-2,
        'train_batch_size': 8000,
        'test_batch_size': 256,
        'drop_ratio': 0.1,
        # sample selection
        'paced_loss': False,
        'paced_loss_slope': False,
        'paced_entropy': False,
        'paced_entropy_slope': False,
        'forget_rate': 0.06,
        'slope_alpha': 0.05,
        'num_gradual': 20, # 20
        # graph settings
        'n_layers': 1,
        'next_hop_num': 1,
        'graph_dropout': False,
        # prob settings
        'entropy_threshold': 1.0,
        'sample_num': 4,
        'sample_loss_weight': 0.01,
        'entropy_num': 4,
        'entropy_reg_weight': 1,
        # SAS settings
        'decay_factor': 0.9,
        'hidden_size': 32,
        'num_hidden_layers': 1,
        'num_attention_heads': 2,
        'intermediate_size': 64,
        'hidden_act': "gelu",
        'hidden_dropout_prob': 0.1,
        'attention_probs_dropout_prob': 0.1,
        'type_vocab_size': 1,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'use_item_bias': True,
        # 'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        # Caser Config
        'n_h': 16,
        'n_v': 4,
        # GRU4Rec config
        'gru_layer_num': 2,
    }

random.seed(123)


def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    config['dataset'] = args.dataset
    config['rec_model'] = args.rec
    config['learning_rate'] = float(args.lr)
    config['weight_decay'] = float(args.l2)
    config['sample_loss_weight'] = float(args.sample)
    for num_attention_head in [2]:
        config['num_attention_heads'] = num_attention_head
        config['intermediate_size'] = num_attention_head * config['hidden_size']
        for noise_type in ['raw']:
            config['noise_type'] = noise_type
            data_model = GraphDataCollector(config)
            graph = data_model.getSparseGraph()
            rec_model_1 = RobGraphSeqRec(config, context_select(config), graph)
            rec_model_2 = RobGraphSeqRec(config, context_select(config), graph)
            print_dict(config, 'config')
            trainer = Trainer(config, rec_model_1, rec_model_2, data_model, None,
                              save_dir='./datasets/' + config['dataset'] + '/seq/')
            trainer.run_co()
            data_model = None
            rec_model_1 = None
            trainer = None










if __name__ == '__main__':
    main()



