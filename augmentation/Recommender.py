from augmentation.BERT_SeqRec import BertForSeqRec
from data_utils.RankingEvaluator import RankingEvaluator
from augmentation.BERT_SeqRec import BertConfig
import torch
from torch import optim
# from torchviz import make_dot

from augmentation.temporary import TemporaryModel
import math

BERT_MODEL = 'bert-base-uncased'
MAX_SEQ_LENGTH = 64


class Recommender:
    def __init__(self, config, model, valid_loader, test_loader):
        # test data: data_collector.test_collection
        # self._model = BertForSeqRec(BertConfig(numItem, config))
        self._model = model

        self._device = config['device']
        self._model.double().to(self._device)

        self._valid_loader = valid_loader
        self._evaluator = RankingEvaluator(test_loader)

        self._optimizer = _get_optimizer(
            self._model, learning_rate=config['rec_learning_rate'])

    def train_one_batch(self, batch, aug_labels=None):
        self._model.train()
        self._optimizer.zero_grad()
        if aug_labels is None:
            overall_loss, clean_loss, noise_loss, sample_idices = self._model(batch)
            aug_loss = torch.tensor(0)
            overall_loss.sum().backward()
        else:
            loss, aug_loss = self._model.forward_aug(batch, aug_labels)
            (loss + aug_loss).backward()
        self._optimizer.step()

        return overall_loss, aug_loss

    def get_valid_loss(self):
        self._model.eval()
        sum_loss = 0.
        for step, batch in enumerate(self._valid_loader):
            loss = self._model(batch)
            sum_loss += loss
        return sum_loss

    def evaluate(self, iter):
        self._model.eval()
        keep_train = self._evaluator.evaluate(model=self._model, train_iter=iter)
        return keep_train

    def update_augmenter(self, batch, augmenter, batch_id):
        augmenter.model.train()
        augmenter.model.zero_grad()
        aug_labels = augmenter.augment_batch(batch, batch_id)
        # aug_labels = [bs], [bs, num_item], [bs, num_item], [bs]
        temp_model = TemporaryModel(self._model)
        self._model.zero_grad()
        # calculate grads on both raw and augmented batches
        raw_loss, aug_loss = self._model.forward_aug(batch, aug_labels)
        if torch.isnan(raw_loss).any():
            print('forward_aug raw_loss NaN')
        if torch.isnan(aug_loss).any():
            print('forward_aug aug_loss NaN')
        raw_loss += aug_loss

        grads = torch.autograd.grad(
            raw_loss, [param for name, param in self._model.named_parameters()],
            create_graph=True)
        for tensor in grads:
            if torch.isnan(tensor).any():
                print(f'grads NaN: {tensor}')
        grads = {param: grads[i] for i, (name, param) in enumerate(
            self._model.named_parameters())}
        # calculate deltas on recommender parameters
        deltas = _adam_delta(self._optimizer, self._model, grads)
        # update deltas to a temporary model
        temp_model.update_params(deltas)

        # compute the performance of the temporary model
        for step, batch in enumerate(self._valid_loader):
            raw_loss = temp_model(batch)
            if torch.isnan(raw_loss).any():
                print(f'temp model loss NaN')
            # backward the gradients
            raw_loss.backward(retain_graph=True)
            # g = make_dot(raw_loss)
            # g.render('test_model', view=True)
        augmenter.optimizer.step()

    @property
    def model(self):
        return self._model

    @property
    def optimizer(self):
        return self._optimizer


def _get_optimizer(model, learning_rate):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if
                    not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
        {'params': [p for n, p in param_optimizer if
                    any(nd in n for nd in no_decay)], 'weight_decay': 0.0}]

    return optim.Adam(optimizer_grouped_parameters, lr=learning_rate)


def _adam_delta(optimizer, model, grads):
    deltas = {}
    for group in optimizer.param_groups:
        for param in group['params']:
            grad = grads[param]
            state = optimizer.state[param]

            exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
            beta1, beta2 = group['betas']

            step = state['step'] + 1

            if group['weight_decay'] != 0:
                grad = grad + group['weight_decay'] * param.data

            exp_avg = exp_avg * beta1 + (1. - beta1) * grad
            exp_avg_sq = exp_avg_sq * beta2 + (1. - beta2) * grad * grad
            denom = exp_avg_sq.sqrt() + group['eps']

            bias_correction1 = 1. - beta1 ** step
            bias_correction2 = 1. - beta2 ** step
            step_size = group['lr'] * math.sqrt(
                bias_correction2) / bias_correction1

            deltas[param] = -step_size * exp_avg / denom

    param_to_name = {param: name for name, param in model.named_parameters()}

    return {param_to_name[param]: delta for param, delta in deltas.items()}