from augmentation.BERT_SeqRec import BertForSeqRec
from data_utils.RankingEvaluator import RankingEvaluator
from augmentation.BERT_SeqRec import BertConfig

import torch
from torch import nn
from torch import optim
from torch.nn import functional as F
from torch.utils.data import Dataset, DataLoader
import numpy as np
import random


BERT_MODEL = 'bert-base-uncased'
MAX_SEQ_LENGTH = 64


class Augmenter:
    def __init__(self, config, model, valid_loader, test_loader, data_model):
        self.model = model
        self.device = config['device']
        self.model.double().to(self.device)

        self.valid_loader = valid_loader
        self.evaluator = RankingEvaluator(test_loader)

        self.optimizer = _get_optimizer(
            self.model, learning_rate=config['rec_learning_rate'])
        self.data_model = data_model

    def get_valid_loss(self):
        self.model.eval()
        sum_loss = 0.
        for step, batch in enumerate(self.valid_loader):
            loss = self.model(batch)
            sum_loss += loss
        return sum_loss

    def train_one_batch(self, batch):
        self.model.train()
        self.optimizer.zero_grad()
        loss = self.model(batch)
        loss.backward()
        self.optimizer.step()
        return loss

    def augment_batch(self, batch, batch_id):
        # [bs, num_item]
        full_pred_scores, raw_weights = self.model.get_fullpred_scores(batch)
        # if torch.isnan(full_pred_scores).any():
        #     print(f'full_pred_score has NaN')
        #     return None
        # if torch.isnan(raw_weights).any():
        #     print(f'raw_weights has NaN')
        #     return None
        # [bs, num_item], one-hot
        pos_augs = gumbel_softmax(full_pred_scores, hard=True)
        # if torch.isnan(pos_augs).any():
        #     print(f'pos_augs has NaN')
        #     return None
        # [bs, num_item], one-hot
        neg_augs = gumbel_softmax(-full_pred_scores, hard=True)
        # if torch.isnan(neg_augs).any():
        #     print(f'neg_augs has NaN')
        #     return None
        # [bs]
        pos_values = torch.mul(full_pred_scores, pos_augs).sum(dim=1, keepdim=False)

        # if torch.isnan(pos_values).any():
        #     print(f'pos_values has NaN')
        #     return None
        # [bs]
        neg_values = torch.mul(full_pred_scores, neg_augs).sum(dim=1, keepdim=False)

        # if torch.isnan(neg_values).any():
        #     print(f'neg_values has NaN')
        #     return None
        # [bs], prob dist
        aug_weights = torch.sigmoid((pos_values - neg_values))
        # if torch.isnan(aug_weights).any():
        #     print(f'aug_weights has NaN')
        #     return None
        if batch_id == 1:
            # print_tensor(pos_augs, 'pos_aug')
            # print_tensor(neg_augs, 'neg_aug')
            print_tensor(pos_values, 'pos_values')
            print_tensor(neg_values, 'neg_values')
            print_tensor(aug_weights, 'aug_weights')
            # print_tensor(raw_weights, 'raw_weights')
            pos_idices = pos_augs.argmax(dim=1, keepdim=False)
            neg_idices = neg_augs.argmax(dim=1, keepdim=False)
            self.aug_quality_eval(users=batch[0],
                                  input_rows=batch[1],
                                  pos_targets=pos_idices,
                                  neg_targets=neg_idices)
        # [bs], [bs, num_item], [bs, num_item], [bs]
        aug_labels = (raw_weights, pos_augs, neg_augs, aug_weights)

        return aug_labels

    def finetune_batch(self, recommender, batch, batch_id):
        # Sets the module in training mode.
        self.model.train()
        # Sets gradients of all model parameters to zero.
        self.model.zero_grad()
        # [bs], [bs, num_item], [bs, num_item], [bs]
        aug_labels = self.augment_batch(batch, batch_id)
        recommender.cal_devloss_for_generator(batch, aug_labels)
        self.optimizer.step()

    def evaluate(self, iter):
        self.model.eval()
        keep_train = self.evaluator.evaluate(model=self.model, train_iter=iter)
        return keep_train

    def aug_quality_eval(self, users, input_rows, pos_targets, neg_targets):
        # [bs, input_len], [bs], [bs]
        idx_range = list(input_rows.size())[0]
        sim_gaps = []
        rank_gaps = []
        pos_sims = []
        neg_sims = []
        pos_ranks = []
        neg_ranks = []
        bad_pos_count = 0
        bad_sample_count = 0
        for i in range(idx_range):
            input_ids = input_rows[i].tolist()
            user = users[i].item()
            pos = pos_targets[i].item()
            neg = neg_targets[i].item()
            pos_sim, neg_sim, pos_rank_ratio, neg_rank_ratio = self.data_model.cal_sim_rank(user, input_ids, pos, neg)
            pos_sims.append(pos_sim)
            neg_sims.append(neg_sim)
            pos_ranks.append(pos_rank_ratio)
            neg_ranks.append(neg_rank_ratio)
            sim_gaps.append(pos_sim - neg_sim)
            rank_gaps.append(neg_rank_ratio - pos_rank_ratio)
            if pos_rank_ratio > self.data_model.threshold:
                bad_pos_count += 1
            if pos_sim < neg_sim:
                bad_sample_count += 1
        pos_sims = torch.tensor(pos_sims)
        neg_sims = torch.tensor(neg_sims)
        pos_ranks = torch.tensor(pos_ranks)
        neg_ranks = torch.tensor(neg_ranks)
        sim_gaps = torch.tensor(sim_gaps)
        rank_gaps = torch.tensor(rank_gaps)
        print(f'Aug Quality: bad pos ratio: {round(bad_pos_count / idx_range, 4)}, '
              f'bad sample ratio: {round(bad_sample_count / idx_range, 4)}')
        print_tensor(pos_sims, 'pos_sims')
        print_tensor(neg_sims, 'neg_sims')
        print_tensor(pos_ranks, 'pos_ranks')
        print_tensor(neg_ranks, 'neg_ranks')
        print_tensor(sim_gaps, 'sim_gaps')
        print_tensor(rank_gaps, 'rank_gaps')

def _get_optimizer(model, learning_rate):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if
                    not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
        {'params': [p for n, p in param_optimizer if
                    any(nd in n for nd in no_decay)], 'weight_decay': 0.0}]

    return optim.Adam(optimizer_grouped_parameters, lr=learning_rate)


def gumbel_softmax(logits, tau=1, hard=False, dim=-1):
    r"""
    Samples from the Gumbel-Softmax distribution (`Link 1`_  `Link 2`_) and optionally discretizes.

    Args:
      logits: `[..., num_features]` unnormalized log probabilities
      tau: non-negative scalar temperature
      hard: if ``True``, the returned samples will be discretized as one-hot vectors,
            but will be differentiated as if it is the soft sample in autograd
      dim (int): A dimension along which softmax will be computed. Default: -1.

    Returns:
      Sampled tensor of same shape as `logits` from the Gumbel-Softmax distribution.
      If ``hard=True``, the returned samples will be one-hot, otherwise they will
      be probability distributions that sum to 1 across `dim`.
    """
    raw_gumbels = -(torch.empty_like(logits).exponential_() + 1e-10).log()   # ~Gumbel(0,1)

    gumbels = (logits + raw_gumbels) / tau  # ~Gumbel(logits,tau)
    y_soft = F.softmax(gumbels, dim=dim)
    # if torch.isnan(y_soft).any():
    #     print(f'y_soft NaN')
    #     print_tensor(raw_gumbels, 'raw_gumbels')
    #     print_tensor(logits, 'logits')
    #     print_tensor(gumbels, 'gumbels')
    #     print_tensor(y_soft, 'y_soft')

    if hard:
        # Straight through.
        index = y_soft.max(dim, keepdim=True)[1]
        y_hard = torch.zeros_like(logits).scatter_(dim, index, 1.0)
        ret = y_hard - y_soft.detach() + y_soft
    else:
        # Reparametrization trick.
        ret = y_soft
    return ret


def print_tensor(tensor, name):
    print(name)
    print(f'min: {round(tensor.min().item(), 4)}, '
          f'max: {round(tensor.max().item(), 4)}, '
          f'mean: {round(tensor.mean().item(), 4)}, '
          f'var: {round(tensor.var().item(), 4)}')
