from augmentation.BERT_SeqRec import BertForSeqRec
from data_utils.RankingEvaluator import RankingEvaluator
from augmentation.BERT_SeqRec import BertConfig
import torch
from torch import optim
# from torchviz import make_dot
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
import numpy as np
import random
from data_utils.SeqDataGenerator import print_single_hist_distribution
from data_utils.SeqDataGenerator import print_double_hist_distribution
from torch.optim.lr_scheduler import ReduceLROnPlateau
from matplotlib.pyplot import MultipleLocator
plt.rcParams['savefig.dpi'] = 200  # 图片像素
plt.rcParams['figure.dpi'] = 200  # 分辨率
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

from augmentation.temporary import TemporaryModel
import math

BERT_MODEL = 'bert-base-uncased'
MAX_SEQ_LENGTH = 64



class Trainer:
    def __init__(self, config, rec_model_1, rec_model_2, data_model, save_dir):

        train_loader, clean_threshold, user_instances, item_in_instances, item_out_instances \
            = data_model.generate_train_dataloader_unidirect()
        test_loader = data_model.generate_test_dataloader_unidirect()

        self.config = config
        self.save_dir = save_dir
        self.save_loss_entropy = config['save_loss_entropy']
        self.train_type = config['train_type']
        self.rec_model = config['rec_model']
        self.noise_type = config['noise_type']

        self.train_loader = train_loader
        self._evaluator_1 = RankingEvaluator(test_loader)
        self._evaluator_2 = RankingEvaluator(test_loader)

        self.user_instances = user_instances
        self.item_in_instances = item_in_instances
        self.item_out_instances = item_out_instances
        self.itemIdx2Str = data_model.itemIdx2Str
        self.itemIdx2Attributes = data_model.itemIdx2Attributes
        self.item_dist = np.array(data_model.item_dist)
        self.user_dist = np.array(data_model.user_dist)

        self.clean_threshold = clean_threshold
        self.train_size = len(train_loader.dataset)
        self.noise_size = self.train_size - self.clean_threshold
        self.instance_loss = [[] for i in range(self.train_size)]
        self.instance_entropy = [[] for i in range(self.train_size)]
        self.loss_slope = np.zeros(self.train_size)
        self.entropy_slope = np.zeros(self.train_size)
        self.last_loss = np.zeros(self.train_size)
        self.last_entropy = np.zeros(self.train_size)

        self.loss_entropy = Loss_Entropy_Loader(config, save_dir='./datasets/' + config['dataset'] + '/seq/',
                                                sample_size=config['analysis_sample_size'],
                                                clean_threshold=clean_threshold,
                                                item_in_instances=item_in_instances,
                                                user_instances=user_instances)

        self.min_loss = 100
        self.min_entropy = 100
        self.max_loss = 0
        self.max_entropy = 0
        self.paced_loss = self.config['paced_loss']
        self.paced_entropy = self.config['paced_entropy']
        self.paced_slope_loss = self.config['paced_loss_slope']
        self.paced_slope_entropy = self.config['paced_entropy_slope']
        self.forget_rate = self.config['forget_rate']
        self.slope_alpha = self.config['slope_alpha']
        self.num_gradual = self.config['num_gradual']
        self.model_save_dir = './datasets/' + self.config['dataset'] + '/model/'
        self.model_save_path = self.model_save_dir + self.rec_model + str(self.config['sample_loss_weight']) + '-'
        self.save_epochs = self.config['save_epochs']

        if self.train_type == 'train':
            if rec_model_1 is not None:
                self._model_1 = rec_model_1
                self._device = config['device']
                self._model_1.double().to(self._device)
                self._optimizer_1 = _get_optimizer(
                    self._model_1, learning_rate=config['learning_rate'], weight_decay=config['weight_decay'])
                self.scheduler_1 = ReduceLROnPlateau(self._optimizer_1, 'max', patience=10,
                                                     factor=config['decay_factor'])
            if rec_model_2 is not None:
                self._model_2 = rec_model_2
                self._model_2.double().to(self._device)
                self._optimizer_2 = _get_optimizer(
                    self._model_2, learning_rate=config['learning_rate'], weight_decay=config['weight_decay'])
                self.scheduler_2 = ReduceLROnPlateau(self._optimizer_2, 'max', patience=10,
                                                     factor=config['decay_factor'])
            else:
                self._model_2 = None
            self.forget_rates = self.build_forget_rates()
        elif self.train_type == 'eval':
            self._device = config['device']
            self._model_1 = rec_model_1

    def build_forget_rates(self):
        forget_rates = np.ones(self.config['epoch_num']) * self.forget_rate
        forget_rates[:self.num_gradual] = np.linspace(0, self.forget_rate, self.num_gradual)
        return forget_rates

    def save_model(self, epoch_num):
        if not os.path.exists(self.model_save_dir):
            os.makedirs(self.model_save_dir)
        save_path = self.model_save_path + str(epoch_num) + '-model.pkl'
        torch.save(self._model_1.context_encoder, save_path)
        print(f'model saved at {save_path}')

    def load_model(self, epoch_num):
        load_path = self.model_save_path + str(epoch_num) + '-model.pkl'
        print(f'loading model from {load_path}')
        return torch.load(load_path)

    def self_train_one_batch(self, batch, epoch_num, batch_num):
        self._model_1.train()
        self._optimizer_1.zero_grad()
        # [bs], [bs], [bs]
        overall_loss, rec_loss, entropy, sample_idices = self._model_1(batch)
        batch_size = overall_loss.size()[0]

        if any((self.paced_loss, self.paced_entropy, self.paced_slope_loss, self.paced_slope_entropy)):
            avail_loss = self.filter_loss(overall_loss, entropy, sample_idices, epoch_num, batch_num)
        else:
            avail_loss = overall_loss

        sumed_avail_loss = avail_loss.sum()
        sumed_avail_loss.backward()
        self._optimizer_1.step()

        if self.save_loss_entropy:
            for i in range(batch_size):
                ins_idx = sample_idices[i].item()
                self.instance_loss[ins_idx].append(overall_loss[i].item())
                self.instance_entropy[ins_idx].append(entropy[i].item())

        return sumed_avail_loss

    def co_train_one_batch(self, batch, epoch_num, batch_num):
        self._model_1.train()
        self._optimizer_1.zero_grad()
        self._model_2.train()
        self._optimizer_2.zero_grad()
        # [bs], [bs], [bs]
        overall_loss_1, rec_loss_1, entropy_1, sample_idices = self._model_1(batch)
        overall_loss_2, rec_loss_2, entropy_2, _ = self._model_2(batch)

        loss_1_update, loss_2_update, coverd_noise_1, coverd_noise_2 = self.co_teaching(
            overall_loss_1, rec_loss_1, entropy_1, overall_loss_2, rec_loss_2, entropy_2, epoch_num, batch_num, sample_idices)

        loss_1_update.backward()
        self._optimizer_1.step()
        loss_2_update.backward()
        self._optimizer_2.step()

        return loss_1_update, loss_2_update, coverd_noise_1, coverd_noise_2

    def co_teaching(self, overall_loss_1, rec_loss_1, entropy_1, overall_loss_2, rec_loss_2, entropy_2, epoch_num, batch_num, sample_idices):
        list_len = len(rec_loss_1)
        num_forget = int(self.forget_rates[epoch_num] * list_len)
        loss_1_kept_bool, ind_1_removed = self.select_valid_overall_loss(rec_loss_1, entropy_1, list_len, num_forget)
        loss_2_kept_bool, ind_2_removed = self.select_valid_overall_loss(rec_loss_2, entropy_2, list_len, num_forget)
        # loss_1_update = overall_loss_1 * loss_2_kept_bool + rating_dist_loss_1 * torch.abs(loss_2_kept_bool - 1)
        loss_1_update = overall_loss_1 * loss_2_kept_bool
        # loss_2_update = overall_loss_2 * loss_1_kept_bool + rating_dist_loss_2 * torch.abs(loss_1_kept_bool - 1)
        loss_2_update = overall_loss_2 * loss_1_kept_bool
        coverd_noise_1, coverd_noise_2 = self.summarize_coverd_noise(sample_idices[ind_1_removed], sample_idices[ind_2_removed], batch_num)

        return loss_1_update.sum(), loss_2_update.sum(), coverd_noise_1, coverd_noise_2
        # return overall_loss_1.sum(), overall_loss_2.sum(), set(), set()

    def select_valid_overall_loss(self, loss, entropy, list_len, num_forget): # remove those
        ind_loss_sorted = np.argsort(loss.cpu().data)  # loss: the higher the better
        ind_entropy_sorted = np.argsort(entropy.cpu().data)  # entropy: the lower the better
        remo_entropy_ind = set(ind_entropy_sorted[0: int(num_forget * self.config['entropy_num'])].tolist())
        if num_forget == 0:
            remo_loss_ind = set()
        else:
            remo_loss_ind = set(ind_loss_sorted[-num_forget:].tolist())
        remo_overall_loss_ind = list(remo_loss_ind.intersection(remo_entropy_ind))
        loss_kept_bool = np.ones(list_len)
        loss_kept_bool[remo_overall_loss_ind] = 0

        return torch.tensor(loss_kept_bool).to(self._device), remo_overall_loss_ind

    def summarize_coverd_noise(self, removed_indices_1, removed_indices_2, batch_num):
        coverd_noise_1 = set()
        coverd_noise_2 = set()
        for i in removed_indices_1:
            if i > self.clean_threshold:
                coverd_noise_1.add(i.item())
        for i in removed_indices_2:
            if i > self.clean_threshold:
                coverd_noise_2.add(i.item())
        if batch_num == 0:
            print('---------CoTeaching info------------------')
            pre_1 = round(len(coverd_noise_1) / (len(removed_indices_1) + 1e-8), 4)
            pre_2 = round(len(coverd_noise_2) / (len(removed_indices_2) + 1e-8), 4)
            print(f'noise_precision_1: {pre_1}, noise_precision_2: {pre_2}')
            covered_noise_over_lap = coverd_noise_1.intersection(coverd_noise_2)
            covered_noise_overlap_ratio_1 = len(covered_noise_over_lap) / (len(coverd_noise_1) + 1e-8)
            covered_noise_overlap_ratio_2 = len(covered_noise_over_lap) / (len(coverd_noise_2) + 1e-8)
            print(f'covered_noise_overlap_1: {covered_noise_overlap_ratio_1}, covered_noise_overlap_2: {covered_noise_overlap_ratio_2}')
            remo_idx_1_set = set(removed_indices_1.tolist())
            remo_idx_2_set = set(removed_indices_2.tolist())
            all_remo_over_lap = remo_idx_1_set.intersection(remo_idx_2_set)
            all_over_lap_ratio = round(len(all_remo_over_lap) / (len(removed_indices_1) + 1e-8), 4)
            print(f'all_remo_overlap: {all_over_lap_ratio}')
            print('-----------------------------------------')

        return coverd_noise_1, coverd_noise_2

    def filter_loss(self, overall_loss, overall_entropy, sample_indices, epoch_num, batch_num):
        remember_rate = 1 - self.forget_rate
        sample_num = overall_loss.shape[0]
        num_remember = int(remember_rate * sample_num)
        overall_kept_bool = np.zeros(overall_loss.size())
        numpy_overall_loss = overall_loss.cpu().detach().numpy()
        numpy_overall_entropy = overall_entropy.cpu().detach().numpy()

        if self.paced_loss:
            sorted_loss_index = np.argsort(numpy_overall_loss)
            kept_loss_index = sorted_loss_index[0: num_remember]
            overall_kept_bool[kept_loss_index] = 1
            if batch_num == 0:
                print(f'loss_kept_num {overall_kept_bool.sum()}')

        if self.paced_entropy:
            sorted_entropy_index = np.argsort(numpy_overall_entropy)
            kept_entropy_index = sorted_entropy_index[0: num_remember]
            overall_kept_bool[kept_entropy_index] = 1
            if batch_num == 0:
                print(f'add_entropy_kept_num {overall_kept_bool.sum()}')

        if self.paced_slope_loss:
            if epoch_num == 0:
                self.last_loss[sample_indices] = numpy_overall_loss
            else:
                curr_loss_slope = np.abs(self.last_loss[sample_indices] - numpy_overall_loss)
                sorted_lslope_index = np.argsort(curr_loss_slope)
                kept_lslope_index = sorted_lslope_index[0: num_remember]
                overall_kept_bool[kept_lslope_index] = 1
                self.loss_slope[sample_indices] = self.slope_alpha * self.loss_slope[sample_indices] + (1 - self.slope_alpha) * curr_loss_slope
                self.last_loss[sample_indices] = numpy_overall_loss
                if batch_num == 0:
                    print(f'add_loss_slope_kept_num {overall_kept_bool.sum()}')

        if self.paced_slope_entropy:
            if epoch_num == 0:
                self.last_entropy[sample_indices] = numpy_overall_entropy
            else:
                curr_entropy_slope = np.abs(self.last_entropy[sample_indices] - numpy_overall_entropy)
                sorted_eslope_index = np.argsort(curr_entropy_slope)
                kept_eslope_index = sorted_eslope_index[0: num_remember]
                overall_kept_bool[kept_eslope_index] = 1
                self.entropy_slope[sample_indices] = self.slope_alpha * self.entropy_slope[sample_indices] + (1 - self.slope_alpha) * curr_entropy_slope
                self.last_entropy[sample_indices] = numpy_overall_entropy
                if batch_num == 0:
                    print(f'add_entropy_slope_kept_num {overall_kept_bool.sum()}')

        if batch_num == 0:
            print(f'final kept num {overall_kept_bool.sum()}')

        available_loss = overall_loss * torch.tensor(overall_kept_bool).to(self._device)
        return available_loss

    def run_self(self):
        if self.train_type == 'train':
            print('=' * 60, '\n', 'Start Training', '\n', '=' * 60, sep='')
            keep_train = True
            for epoch in range(self.config['epoch_num']):
                start = time.time()
                loss_iter = 0
                for i, batch in enumerate(self.train_loader):
                    overall_loss = self.self_train_one_batch(batch, epoch, i)
                    loss_iter += overall_loss.item()
                print(f"epoch: {epoch}, "
                      f"loss: {round(loss_iter / len(self.train_loader), 4)}, time: {round(time.time() - start, 5)}")
                keep_train = self.evaluate(epoch)
                if not keep_train:
                    break
            if self.save_loss_entropy:
                self.save_str_loss_entropy_data()
        else:  # analysis
            self.load_str_loss_entropy_data()
            self.loss_entropy_analysis()

    def run_co(self):
        if self.train_type == 'train':
            print('=' * 60, '\n', 'Start Training', '\n', '=' * 60, sep='')
            keep_train = True
            for epoch in range(self.config['epoch_num']):
                start = time.time()
                loss_1_iter = 0
                loss_2_iter = 0
                coverd_noise_1 = set()
                coverd_noise_2 = set()
                for i, batch in enumerate(self.train_loader):
                    loss_1, loss_2, coverd_noise_1_batch, coverd_noise_2_batch = self.co_train_one_batch(batch, epoch, i)
                    coverd_noise_1 = coverd_noise_1.union(coverd_noise_1_batch)
                    coverd_noise_2 = coverd_noise_2.union(coverd_noise_2_batch)
                    loss_1_iter += loss_1.item()
                    loss_2_iter += loss_2.item()
                noise_overlap = coverd_noise_1.intersection(coverd_noise_2)
                print(f'################## epoch {epoch} ###########################')
                print(f"loss_1: {round(loss_1_iter / len(self.train_loader), 4)}, "
                      f"loss_2: {round(loss_2_iter / len(self.train_loader), 4)}, time: {round(time.time() - start, 5)} \n"
                      f"noise_coverage_1: {round(len(coverd_noise_1) / (self.noise_size + 1e-8), 4)}, "
                      f"noise_coverage_2: {round(len(coverd_noise_2) / (self.noise_size + 1e-8), 4)}\n"
                      f"noise_overlap_1: {round(len(noise_overlap) / (len(coverd_noise_1) + 1e-8), 4)}, "
                      f"noise_overlap_2: {round(len(noise_overlap) / (len(coverd_noise_2) + 1e-8), 4)}")
                keep_train = self.evaluate(epoch)
                print('#########################################################')
                if epoch in self.save_epochs:
                    self.save_model(epoch)
                if not keep_train:
                    break
            if self.save_loss_entropy:
                self.save_str_loss_entropy_data()
        elif self.train_type == 'analysis':  # analysis
            self.load_str_loss_entropy_data()
            self.loss_entropy_analysis(sample_size=self.config['analysis_sample_size'])
        elif self.train_type == 'eval':
            for epoch in self.save_epochs:
                self._model_1.set_encoder(self.load_model(epoch))
                self._model_1.double().to(self._device)
                for i, batch in enumerate(self.train_loader):
                    overall_loss, rec_loss, entropy, sample_idices = self._model_1(batch)
                    batch_size = overall_loss.size()[0]
                    for j in range(batch_size):
                        ins_idx = sample_idices[j].item()
                        self.instance_loss[ins_idx] = rec_loss[j].item()
                        self.instance_entropy[ins_idx] = entropy[j].item()
                self._evaluator_1.evaluate(model=self._model_1, train_iter=0)
                scatter(self.instance_loss, self.instance_entropy, forget_rate=0.1,
                        save_path=self.save_dir + '/case_study-' + str(self.config['sample_loss_weight']) + '-' + str(epoch))

    def run_co_prob(self):
        if self.train_type == 'train':
            print('=' * 60, '\n', 'Start Training', '\n', '=' * 60, sep='')
            keep_train = True
            for epoch in range(self.config['epoch_num']):
                start = time.time()
                loss_1_iter = 0
                loss_2_iter = 0
                coverd_noise_1 = set()
                coverd_noise_2 = set()
                for i, batch in enumerate(self.train_loader):
                    loss_1, loss_2, coverd_noise_1_batch, coverd_noise_2_batch = self.co_train_one_batch(batch, epoch, i)
                    coverd_noise_1 = coverd_noise_1.union(coverd_noise_1_batch)
                    coverd_noise_2 = coverd_noise_2.union(coverd_noise_2_batch)
                    loss_1_iter += loss_1.item()
                    loss_2_iter += loss_2.item()
                noise_overlap = coverd_noise_1.intersection(coverd_noise_2)
                print(f'################## epoch {epoch} ###########################')
                print(f"loss_1: {round(loss_1_iter / len(self.train_loader), 4)}, "
                      f"loss_2: {round(loss_2_iter / len(self.train_loader), 4)}, time: {round(time.time() - start, 5)} \n"
                      f"noise_coverage_1: {round(len(coverd_noise_1) / (self.noise_size + 1e-8), 4)}, "
                      f"noise_coverage_2: {round(len(coverd_noise_2) / (self.noise_size + 1e-8), 4)}\n"
                      f"noise_overlap_1: {round(len(noise_overlap) / (len(coverd_noise_1) + 1e-8), 4)}, "
                      f"noise_overlap_2: {round(len(noise_overlap) / (len(coverd_noise_2) + 1e-8), 4)}")
                keep_train = self.evaluate(epoch)
                print('#########################################################')
                if not keep_train:
                    break
            if self.save_loss_entropy:
                self.save_str_loss_entropy_data()
        else:  # analysis
            self.load_str_loss_entropy_data()
            self.loss_entropy_analysis(sample_size=self.config['analysis_sample_size'])

    def evaluate(self, iter):
        self._model_1.eval()
        print('---------------eval model 1-----------------')
        keep_train_1, ndcg10_1 = self._evaluator_1.evaluate(model=self._model_1, train_iter=iter)
        self.scheduler_1.step(ndcg10_1)
        keep_train_2 = False
        if self._model_2 is not None:
            print('---------------eval model 2-----------------')
            self._model_2.eval()
            keep_train_2, ndcg10_2 = self._evaluator_2.evaluate(model=self._model_2, train_iter=iter)
            self.scheduler_2.step(ndcg10_2)
        return any((keep_train_1, keep_train_2))

    @property
    def model(self):
        return self._model_1

    @property
    def optimizer(self):
        return self._optimizer_1

    def save_str_loss_entropy_data(self):
        file_name = self.noise_type + '_' + self.config['rec_model']
        print(f'save detailed loss and entropy for {file_name}')
        loss_lines = []
        entropy_lines = []
        for i in range(len(self.instance_loss)):
            loss_list = self.instance_loss[i]
            loss_lines.append(f'{convert_float_list_to_str(loss_list)}\n')
            entropy_list = self.instance_entropy[i]
            entropy_lines.append(f'{convert_float_list_to_str(entropy_list)}\n')
        loss_path = self.save_dir + f'/{file_name}_loss.txt'
        with open(loss_path, 'w', encoding='utf-8') as fout:
            fout.writelines(loss_lines)
        entropy_path = self.save_dir + f'/{file_name}_entropy.txt'
        with open(entropy_path, 'w', encoding='utf-8') as fout:
            fout.writelines(entropy_lines)

    def load_str_loss_entropy_data(self):
        print('get loss and entropy')
        self.clean_loss = self.loss_entropy.clean_loss
        self.noisy_loss = self.loss_entropy.noisy_loss
        self.clean_entropy = self.loss_entropy.clean_entropy
        self.noisy_entropy = self.loss_entropy.noisy_entropy
        self.uncertain_losses = self.loss_entropy.uncertain_losses
        self.pure_losses = self.loss_entropy.pure_losses
        self.min_loss = self.loss_entropy.min_loss
        self.max_loss = self.loss_entropy.max_loss
        self.min_entropy = self.loss_entropy.min_entropy
        self.max_entropy = self.loss_entropy.max_entropy

    def loss_entropy_analysis(self):
        save_dir = self.save_dir + 'fig/' + self.config['rec_model'] + '/sample_' + str(self.config['analysis_sample_size']) + 'iter_' + \
                   str(self.config['analysis_iter_length'])
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        # instance level analysis

        # self.instance_analysis_2D(self.instance_loss, 'Loss',
        #                           self.min_loss, self.max_loss, clean_indices, noisy_indices, save_dir)
        # self.instance_analysis_2D(self.instance_entropy, 'Entropy',
        #                           self.min_entropy, self.max_entropy, clean_indices, noisy_indices, save_dir)
        # self.instance_analysis_3D("Loss_Entropy", self.min_loss, self.max_loss,
        #                           self.min_entropy, self.max_entropy, clean_indices, noisy_indices, save_dir)

        self.hist_3d_dist(self.clean_loss, self.noisy_loss, 'Loss', save_dir)
        self.hist_3d_dist(self.pure_losses, self.uncertain_losses, 'Uncertainty_loss', save_dir=save_dir, noisy_color='darkorange')

        # self.hist_3d_dist(self.instance_entropy, clean_indices, noisy_indices, 'Entropy', save_dir)
        # loss_moving_rms = self.build_moving_rms(data_source=self.instance_loss)
        # entropy_moving_rms = self.build_moving_rms(data_source=self.instance_entropy)
        # loss_moving_slope = self.build_moving_slope(data_source=self.instance_loss)
        # entropy_moving_slope = self.build_moving_slope(data_source=self.instance_entropy)
        # # self.hist_3d_dist(loss_moving_rms, clean_indices, noisy_indices, 'loss_moving_rms', save_dir)
        # # self.hist_3d_dist(entropy_moving_rms, clean_indices, noisy_indices, 'entropy_moving_rms', save_dir)
        # self.hist_3d_dist(loss_moving_slope, clean_indices, noisy_indices, 'loss_moving_slope', save_dir)
        # self.hist_3d_dist(entropy_moving_slope, clean_indices, noisy_indices, 'entropy_moving_slope', save_dir)
        # self.roughness_analysis(self.instance_loss, 'Loss', clean_indices, noisy_indices, save_dir)
        # self.roughness_analysis(self.instance_entropy, 'Entropy', clean_indices, noisy_indices, save_dir)

        # detailed user/item analysis
        # n_user, n_user_ins, user_uncertainy = self.user_or_item_analysis(data=self.user_instances, name='user', save_dir=save_dir)
        # n_item_in, n_item_in_ins, item_uncertainy = self.user_or_item_analysis(data=self.item_in_instances, name='item', save_dir=save_dir)
        n_ui_in, n_ui_in_ins, ui_uncertainy = self.user_and_item_analysis(data_ui=[self.item_in_instances, self.user_instances], name='user_item', save_dir=save_dir)

        # print_double_hist_distribution(clean_data=user_uncertainy, noisy_data=item_uncertainy,
        #                                title='user_item_uncertainty_dist',
        #                                save_dir=save_dir, x_label='uncertainty value', bins=200, y_label=f'#user / #item')
        # print('!!! user item uncertainty dist finished !!!')
        # # n_item_out, n_item_out_ins = self.user_item_analysis(data=self.item_out_instances, name='item-out', save_dir=save_dir)
        # noisy_user_ratio = len(n_user) / len(self.user_instances)
        # noisy_item_ratio = len(n_item_in) / len(self.item_in_instances)
        # print(f'noisy user ratio: {noisy_user_ratio}')
        # print(f'noisy item ratio: {noisy_item_ratio}')

    def build_moving_rms(self, data_source):
        # data_source is loss or entropy
        # output moving rms [sample_num, epoch_num]
        sample_num = data_source.shape[0]
        epoch_num = data_source.shape[1]
        moving_rms = np.zeros([sample_num, epoch_num - 2])
        for i in range(2, epoch_num):
            moving_rms[:, i-2] = data_source[:, 0:i].std(axis=1)
        return moving_rms

    def build_moving_slope(self, data_source):
        sample_num = data_source.shape[0]
        epoch_num = data_source.shape[1]
        moving_slope = np.zeros([sample_num, epoch_num])
        for i in range(epoch_num):
            if i == 0:
                moving_slope[:, i] = data_source[:, i]
            else:
                moving_slope[:, i] = np.abs(data_source[:, i] - data_source[:, i-1])
        return moving_slope



    def instance_analysis_2D(self, data, name, y_min, y_max, clean_indices, noisy_indices, save_dir):
        print(f'analyzing instance 2D {name}')
        clean_data_path = save_dir + f'/{self.rec_model}_clean_{self.noise_type}_{name}.png'
        noisy_data_path = save_dir + f'/{self.rec_model}_noisy_{self.noise_type}_{name}.png'
        plot_instances_2D(data, clean_indices, 'royalblue', 'blue', name, y_min, y_max, clean_data_path)
        plot_instances_2D(data, noisy_indices, 'mediumvioletred', 'red', name, y_min, y_max, noisy_data_path)

    def instance_analysis_3D(self, name, y_min, y_max, z_min, z_max, clean_indices, noisy_indices, save_dir):
        print(f'analyzing instance 3D {name}')
        clean_data_path = save_dir + f'/{self.rec_model}_clean_{self.noise_type}_{name}.png'
        noisy_data_path = save_dir + f'/{self.rec_model}_noisy_{self.noise_type}_{name}.png'
        self.plot_instances_3D(clean_indices, 'royalblue', y_min, y_max, z_min, z_max, clean_data_path)
        self.plot_instances_3D(noisy_indices, 'mediumvioletred', y_min, y_max, z_min, z_max, noisy_data_path)

    def user_or_item_analysis(self, data, name, save_dir):
        print(f'{name} analysis')
        noisy_ratio_dict = {}
        noisy_heads = set()
        noisy_heads_instances = set()
        noise_related_clean_indices = set()
        pure_clean_indices = set()
        for head, inst_idices in data.items():
            noisy_count = 0
            local_clean_set = set()
            for idx in inst_idices:
                if idx > self.clean_threshold:
                    noisy_count += 1
                else:
                    local_clean_set.add(idx)
            noise_ratio = noisy_count / len(inst_idices)
            noisy_ratio_dict[head] = noise_ratio
            if noise_ratio > 0.3:
                if 'item' in name and head == 0:
                    continue
                noise_related_clean_indices = noise_related_clean_indices | local_clean_set
                noisy_heads.add(head)
                noisy_heads_instances = noisy_heads_instances | set(inst_idices)
            else:
                pure_clean_indices = pure_clean_indices | local_clean_set
        if len(noisy_heads) == 0:
            print(f'{name} no noisy object (ratio > 0.3)')
            return noisy_heads, noisy_heads_instances
        related_indices = list(noise_related_clean_indices)
        pure_indices = random.sample(pure_clean_indices, k=len(noise_related_clean_indices))

        # epoch_num = self.instance_loss.shape[1]
        # for epoch_i in range(epoch_num):
        self.hist_3d_dist(data_source=self.instance_loss, clean_indices=pure_indices, noisy_indices=related_indices, title=f'uncertain_{name}', save_dir=save_dir, noisy_color='darkorange')
            # pure_data = clean_data[:, epoch_i].reshape([-1])
            # related_data = noisy_data[:, epoch_i].reshape([-1])
            # print_double_hist_distribution(clean_data=pure_data,
            #                                noisy_data=related_data,
            #                                title=f'{self.noise_type}_{name}_pure_related_comparison_{epoch_i}',
            #                                save_dir=save_dir, x_label='loss',
            #                                bins=200, x_min=min(pure_data.min(), related_data.min()),
            #                                x_max=1.0)
        print_single_hist_distribution(data=np.array(list(noisy_ratio_dict.values())),
                                       title=f'{self.noise_type}_{name}_noise_distribution',
                                       save_dir=save_dir, x_label='uncertainty value', bins=200, y_label=f'#{name}')

        # real_clean_idices = self.sample_clean_instance(noisy_heads_instances)
        # save_path_Loss_noisy = save_dir + f'/{self.rec_model}_{self.noise_type}_{name}_Loss_noisy.png'
        # plot_instances_2D(self.instance_loss, list(noisy_heads_instances),
        #                   'mediumvioletred', 'red', name + '_Loss_noisy', self.min_loss, self.max_loss,
        #                   save_path_Loss_noisy)
        # save_path_Loss_clean = save_dir + f'/{self.rec_model}_{self.noise_type}_{name}_Loss_clean.png'
        # plot_instances_2D(self.instance_loss, list(real_clean_idices),
        #                   'royalblue', 'blue', name + '_Loss_clean', self.min_loss, self.max_loss, save_path_Loss_clean)
        # save_path_Entropy_noisy = save_dir + f'/{self.rec_model}_{self.noise_type}_{name}_Entropy_noisy.png'
        # plot_instances_2D(self.instance_entropy, list(noisy_heads_instances),
        #                   'mediumvioletred', 'red', name + '_Entropy_noisy', self.min_entropy, self.max_entropy,
        #                   save_path_Entropy_noisy)
        # save_path_Entropy_clean = save_dir + f'/{self.rec_model}_{self.noise_type}_{name}_Entropy_clean.png'
        # plot_instances_2D(self.instance_entropy, list(real_clean_idices),
        #                   'royalblue', 'blue', name + '_Entropy_clean', self.min_entropy, self.max_entropy,
        #                   save_path_Entropy_clean)
        # self.roughness_analysis(self.instance_loss, name + '_loss', real_clean_idices, noisy_heads_instances, save_dir)
        # self.roughness_analysis(self.instance_entropy, name + '_Entropy', real_clean_idices, noisy_heads_instances,
        #                         save_dir)
        return noisy_heads, noisy_heads_instances, np.array(list(noisy_ratio_dict.values()))

    # def user_and_item_analysis(self, data_ui, name, save_dir):
    #
    #
    #
    #     print_single_hist_distribution(data=np.array(list(noisy_ratio_dict.values())),
    #                                    title=f'{self.noise_type}_{name}_noise_distribution',
    #                                    save_dir=save_dir, x_label='uncertainty value', bins=200, y_label=f'#{name}')
    #     return noisy_heads, noisy_heads_instances, np.array(list(noisy_ratio_dict.values()))

    def print_noisy_user_item(self, noisy_head, data, name):
        print(f'print {name} details')
        output_lines = []
        for head in noisy_head:
            output_lines.append('------------------------------')
            output_lines.append(f'{name}: {head}\n')
            instance_indices = data[head]
            for instance in instance_indices:
                output_lines.append(self.print_an_instance(instance))
        save_path = self.save_dir + f'/{self.rec_model}_{self.noise_type}_{name}_detail.txt'
        with open(save_path, 'w', encoding='utf-8') as fout:
            fout.writelines(output_lines)

    def print_an_instance(self, instanceIdx):
        user = self.train_loader.dataset.train_users[instanceIdx]
        input_items = self.train_loader.dataset.train_hist_items[instanceIdx]
        target = self.train_loader.dataset.train_targets[instanceIdx]
        print_str = f'User:{user}\nInputs: \n'
        for in_item in input_items:
            print_str += self.print_an_item(in_item) + '\n'

        if instanceIdx > self.clean_threshold:
            print_str += 'Noisy Target:\n'
        else:
            print_str += 'Target:\n'
        print_str += self.print_an_item(target) + '\n\n'
        return print_str

    def print_an_item(self, itemIdx):
        if itemIdx == 0:
            return 'zero pad'
        else:
            return f'{itemIdx}\t{self.itemIdx2Str[itemIdx]}\t{set2str(self.itemIdx2Attributes[itemIdx])}'

    def sample_clean_instance(self, noisy_set):
        sample_size = len(noisy_set)
        clean_sample_size = min(sample_size, self.clean_threshold)
        clean_indices = set(random.sample(range(0, self.clean_threshold), clean_sample_size))
        real_clean_idices = set()
        for idx in clean_indices:
            if idx not in noisy_set:
                real_clean_idices.add(idx)
        while len(real_clean_idices) < sample_size:
            new_insatnce = random.sample(range(0, self.clean_threshold), 1)[0]
            if new_insatnce not in noisy_set:
                real_clean_idices.add(new_insatnce)
        return real_clean_idices

    def roughness_analysis(self, data, name, clean_idices, noisy_idices, save_dir):
        print(f'{name} roughness analysis')
        rms_rough_clean = []
        slope_rough_clean = []
        for i in clean_idices:
            rms_rough_clean.append(RMS_Roughness(data[i]))
            slope_rough_clean.append(mean_slope(data[i]))

        rms_rough_noisy = []
        slope_rough_noisy = []
        for i in noisy_idices:
            rms_rough_noisy.append(RMS_Roughness(data[i]))
            slope_rough_noisy.append(mean_slope(data[i]))

        rms_rough_clean = np.array(rms_rough_clean)
        rms_rough_noisy = np.array(rms_rough_noisy)
        slope_rough_clean = np.array(slope_rough_clean)
        slope_rough_noisy = np.array(slope_rough_noisy)
        if self.noise_type != 'no':
            rms_min = min(rms_rough_clean.min(), rms_rough_noisy.min())
            rms_max = min(rms_rough_clean.max(), rms_rough_noisy.max())
            slope_min = min(slope_rough_clean.min(), slope_rough_noisy.min())
            slope_max = min(slope_rough_clean.max(), slope_rough_noisy.max())
        else:
            rms_min = rms_rough_clean.min()
            rms_max = rms_rough_clean.max()
            slope_min = slope_rough_clean.min()
            slope_max = slope_rough_clean.max()

        print_double_hist_distribution(clean_data=np.array(rms_rough_clean),
                                       noisy_data=np.array(rms_rough_noisy),
                                       title=f'{self.noise_type}_{name}_rms_rough',
                                       save_dir=save_dir, x_label='rms_rough',
                                       bins=200, x_min=rms_min, x_max=rms_max)
        print_double_hist_distribution(clean_data=np.array(slope_rough_clean),
                                       noisy_data=np.array(slope_rough_noisy),
                                       title=f'{self.noise_type}_{name}_slope_rough',
                                       save_dir=save_dir, x_label='slope_rough',
                                       bins=200, x_min=slope_min, x_max=slope_max)

    def plot_instances_3D(self, indices, color, y_min, y_max, z_min, z_max, save_path):
        f = plt.figure()
        ax = Axes3D(f)
        for i in indices:
            loss_list = self.instance_loss[i]
            entropy_list = self.instance_entropy[i]
            ax.plot3D(range(len(loss_list)), loss_list, entropy_list, color=color, linewidth=0.1)
        ax.set_xlabel('Iteration')
        ax.set_ylabel('Loss')
        ax.set_zlabel('Entropy')
        ax.set_ylim(y_min, y_max)
        ax.set_zlim(z_min, z_max)

        f.savefig(save_path)
        plt.close()

    def hist_3d_dist(self, clean_data, noisy_data, title, save_dir, noisy_color='mediumvioletred'):
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        import numpy as np
        print(f'3d dist :{title}')
        fig = plt.figure(dpi=300)
        # plt.rcParams['figure.figsize'] = (8, 6)
        ax = fig.add_subplot(1, 1, 1, projection='3d')
        ax.tick_params(axis='x', which='major', labelsize=12)
        ax.tick_params(axis='y', which='major', labelsize=12)
        ax.tick_params(axis='z', which='major', labelsize=12)
        from matplotlib import ticker
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True)
        formatter.set_powerlimits((-1, 1))
        ax.zaxis.set_major_formatter(formatter)
        # plt.ticklabel_format(axis="z", style="sci", scilimits=(0, 0))
        nbins = 100
        epoch_num = clean_data.shape[1]
        loss_min = 100
        loss_max = 0
        for epoch_i in range(epoch_num):
            if epoch_i % 20 == 0:
                clean_losses = clean_data[:, epoch_i].reshape([-1])
                # clean_losses = np.random.rand(len(clean_indices))
                clean_hist, clean_bins = np.histogram(clean_losses, bins=nbins)
                clean_xs = (clean_bins[:-1] + clean_bins[1:]) / 2 * 10
                ax.bar(clean_xs, clean_hist, zs=epoch_i, zdir='y', color='royalblue', ec='royalblue', alpha=0.7)

                noisy_losses = noisy_data[:, epoch_i].reshape([-1])
                noisy_hist, noisy_bins = np.histogram(noisy_losses, bins=nbins)
                noisy_xs = (noisy_bins[:-1] + noisy_bins[1:]) / 2 * 10
                ax.bar(noisy_xs, noisy_hist, zs=epoch_i, zdir='y', color=noisy_color, ec=noisy_color, alpha=0.6)

                local_loss_min = min(min(clean_losses), min(noisy_losses)) * 10
                local_loss_max = max(max(clean_losses), max(noisy_losses)) * 10

                if local_loss_min < loss_min:
                    loss_min = local_loss_min
                if local_loss_max > loss_max:
                    loss_max = local_loss_max
                #
                print_double_hist_distribution(clean_data=clean_losses,
                                               noisy_data=noisy_losses,
                                               title=f'{title}_epoch {epoch_i}',
                                               save_dir=save_dir, x_label='Loss',
                                               bins=25, x_min=local_loss_min, x_max=local_loss_max, legend_1='reliable', legend_2='unreliable', epoch=epoch_i, y_label='#instance')
        ax.set_xlabel('Loss', fontsize=17)
        plt.xticks([0, 10, 20, 30, 40, 50, 60], labels=['0', '1', '2', '3', '4', '5', '6']) # for ml1m
        # ax.set_zticks([0, 5000, 10000, 15000, 20000])
        # plt.xticks([0, 5, 10, 15, 20, 25, 30], labels=['0', '0.5', '1', '1.5', '2', '2.5', '3'])
        ax.set_xlim(loss_min, loss_max)
        ax.set_ylabel('epoch', fontsize=17)
        ax.yaxis.set_major_locator(MultipleLocator(20))
        ax.set_zlabel('#instance', fontsize=17)

        data_path = save_dir + f'/3d_dist_{title}_{self.noise_type}.png'
        fig.savefig(data_path)
        plt.close()

    def hist_3d_to_2d_dist(self, data_source, clean_indices, noisy_indices, title, save_dir, noisy_color='mediumvioletred'):
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        import numpy as np
        print(f'3d dist :{title}')
        fig = plt.figure(dpi=300)
        # plt.rcParams['figure.figsize'] = (8, 6)
        ax = fig.add_subplot(1, 1, 1, projection='3d')
        ax.tick_params(axis='x', which='major', labelsize=14)
        ax.tick_params(axis='y', which='major', labelsize=14)
        ax.tick_params(axis='z', which='major', labelsize=14)
        from matplotlib import ticker
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True)
        formatter.set_powerlimits((-1, 1))
        ax.zaxis.set_major_formatter(formatter)
        # plt.ticklabel_format(axis="z", style="sci", scilimits=(0, 0))
        nbins = 100
        clean_data = data_source[clean_indices] # [num_clean, num_epoch]
        noisy_data = data_source[noisy_indices] # [num_noisy, num_epoch]
        epoch_num = clean_data.shape[1]
        loss_min = 100
        loss_max = 0
        for epoch_i in range(epoch_num):
            if epoch_i % 20 == 0:
                clean_losses = clean_data[:, epoch_i].reshape([-1])
                # clean_losses = np.random.rand(len(clean_indices))
                clean_hist, clean_bins = np.histogram(clean_losses, bins=nbins)
                clean_xs = (clean_bins[:-1] + clean_bins[1:]) / 2 * 10
                ax.bar(clean_xs, clean_hist, zs=epoch_i, zdir='y', color='royalblue', ec='royalblue', alpha=0.7)

                noisy_losses = noisy_data[:, epoch_i].reshape([-1])
                noisy_hist, noisy_bins = np.histogram(noisy_losses, bins=nbins)
                noisy_xs = (noisy_bins[:-1] + noisy_bins[1:]) / 2 * 10
                ax.bar(noisy_xs, noisy_hist, zs=epoch_i, zdir='y', color=noisy_color, ec=noisy_color, alpha=0.6)

                local_loss_min = min(min(clean_losses), min(noisy_losses)) * 10
                local_loss_max = max(max(clean_losses), max(noisy_losses)) * 10

                if local_loss_min < loss_min:
                    loss_min = local_loss_min
                if local_loss_max > loss_max:
                    loss_max = local_loss_max
                #
                print_double_hist_distribution(clean_data=clean_losses,
                                               noisy_data=noisy_losses,
                                               title=f'{self.noise_type}_{title}_epoch{epoch_i}',
                                               save_dir=save_dir, x_label='rms_rough',
                                               bins=100, x_min=local_loss_min, x_max=local_loss_max)

        ax.set_xlabel('Loss', fontsize=18)
        plt.xticks([0, 10, 20, 30, 40, 50, 60], labels=['0', '1', '2', '3', '4', '5', '6'])
        # ax.set_zticks([0, 5000, 10000, 15000, 20000])
        # plt.xticks([0, 5, 10, 15, 20, 25, 30], labels=['0', '0.5', '1', '1.5', '2', '2.5', '3'])
        ax.set_xlim(loss_min, loss_max)
        ax.set_ylabel('epoch', fontsize=18)
        ax.yaxis.set_major_locator(MultipleLocator(20))
        ax.set_zlabel('#instance', fontsize=18)

        data_path = save_dir + f'/3d_dist_{title}_{self.noise_type}.png'
        fig.savefig(data_path)
        plt.close()


def scatter(loss, entropy, forget_rate, save_path, y_min=-20, y_max=30, x_min=0, x_max=4):
    import matplotlib.pyplot as plt
    plt.rcParams['savefig.dpi'] = 200  # 图片像素
    plt.rcParams['figure.dpi'] = 200  # 分辨率
    plt.rcParams['figure.figsize'] = (6, 4)  # 尺寸

    from matplotlib import rcParams
    rcParams.update({'figure.autolayout': True})
    # plt.xticks(fontsize=19)
    # plt.xticks(fontsize=19)

    loss = np.array(loss)
    entropy = np.array(entropy)
    print('plotting instances loss and uncertainty')

    f, ax = plt.subplots()
    ax.tick_params(axis='both', which='major', labelsize=19)
    ind_loss_sorted = np.argsort(loss)  # loss: the higher the better
    ind_entropy_sorted = np.argsort(entropy)  # entropy: the lower the better
    num_forget = int(forget_rate * len(loss))
    remo_loss_ind = set(ind_loss_sorted[-num_forget:].tolist())
    remo_entropy_ind = set(ind_entropy_sorted[0: int(num_forget) * 5].tolist())
    unreliable_ind = list(remo_loss_ind.intersection(remo_entropy_ind))

    ax.scatter(loss, entropy, c='royalblue', marker='o', s=0.02)
    ax.scatter(loss[unreliable_ind], entropy[unreliable_ind], c='mediumvioletred', marker='o', s=0.02)

    ax.set_ylim(y_min, y_max)
    ax.set_xlim(x_min, x_max)
    ax.set_xlabel('Loss', fontsize=21)
    ax.set_ylabel('Uncertainty', fontsize=21)

    unreliable_ratio = round(len(unreliable_ind) / len(loss), 4)
    f.savefig(save_path + f'-{unreliable_ratio}.png')
    plt.close()


    return unreliable_ratio


def convert_float_list_to_str(list):
    string = ''
    for item in list:
        string += str(round(item, 4)) + ','
    return string


def convert_str_to_float_list(string):
    str_list = string.strip().split(',')[0:-1]
    float_list = [float(item) for item in str_list]

    return float_list


def plot_mean_std(ax, data, color):
    mean = data.mean(axis=0)
    std = data.std(axis=0)
    ax.plot(range(data.shape[-1]), mean, color=color, label='mean')
    r1 = mean - std
    r2 = mean + std
    # ax.plot(range(data.shape[-1]), r1, color=color, alpha=0.4, label='mean-std')
    # ax.plot(range(data.shape[-1]), r2, color=color, alpha=0.7, label='mean+std')
    ax.fill_between(range(data.shape[-1]), r1, r2, color=color, alpha=0.3)
    ax.legend()


def plot_instances_2D(data, indices, color, mean_color, name, y_min, y_max, save_path):
    f, ax = plt.subplots()
    for i in indices:
        value_list = data[i]
        ax.plot(range(len(value_list)), value_list, color=color, linewidth=0.1)

    used_data = np.array(data[indices])
    plot_mean_std(ax, used_data, color=mean_color)
    ax.set_ylim(y_min, y_max)
    ax.set_xlabel('Iteration')
    ax.set_ylabel(name)
    plt.text(x=data.shape[1] * 0.7, y=y_max * 0.7, s=f'#curve:{len(indices)}')
    f.savefig(save_path)
    plt.close()


def RMS_Roughness(data_list):
    """
    a numpy array of loss or entropy, e.g., [0.5, 0.3, 0.1, 0.05]
    """
    return data_list.std()


def mean_slope(data_list):
    """
    absolute slope
    """
    left_shifted_data = data_list[1:]
    raw_data = data_list[0: -1]
    minused_data = np.abs((left_shifted_data - raw_data))

    return minused_data.mean()


def tan(data_list):
    R_q = RMS_Roughness(data_list)
    N = data_list.shape[0]
    Y_sum = np.power(data_list - data_list.mean(), 4).sum()
    return Y_sum / (N * np.power(R_q, 4))


def _get_optimizer(model, learning_rate, weight_decay=0.01):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if
                    not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
        {'params': [p for n, p in param_optimizer if
                    any(nd in n for nd in no_decay)], 'weight_decay': 0.0}]

    return optim.Adam(optimizer_grouped_parameters, lr=learning_rate)


def set2str(input_set):
    set_str = ''
    set_len = len(input_set)
    for i, item in enumerate(input_set):
        if i < set_len - 1:
            set_str += str(item) + ','
        else:
            set_str += str(item)
    return set_str


class Loss_Entropy_Loader:

    def __init__(self, config, save_dir, sample_size, clean_threshold, item_in_instances, user_instances):
        self.config = config
        self.save_dir = save_dir
        self.save_loss_entropy = config['save_loss_entropy']
        self.train_type = config['train_type']
        self.rec_model = config['rec_model']
        self.noise_type = config['noise_type']

        self.instance_loss = None
        self.instance_entropy = None
        self.min_loss = None
        self.max_loss = None
        self.min_entropy = None
        self.max_entropy = None

        if self.train_type == 'analysis':
            uncertain_indices, pure_indices = self.user_and_item_analysis([item_in_instances, user_instances], clean_threshold)
            self.load_str_loss_entropy_data(sample_size, clean_threshold, uncertain_indices, pure_indices)

    def user_and_item_analysis(self, data_ui, clean_threshold):
        print(f'user and item analysis')
        noisy_ratio_dict = {}
        noisy_heads = set()
        noisy_heads_instances = set()
        noise_related_clean_indices = set()
        pure_clean_indices = set()

        for data in data_ui:
            for head, inst_idices in data.items():
                noisy_count = 0
                local_clean_set = set()
                for idx in inst_idices:
                    if idx > clean_threshold:
                        noisy_count += 1
                    else:
                        local_clean_set.add(idx)
                noise_ratio = noisy_count / len(inst_idices)
                noisy_ratio_dict[head] = noise_ratio
                if noise_ratio > 0.3:
                    if head == 0:
                        continue
                    noise_related_clean_indices = noise_related_clean_indices | local_clean_set
                    noisy_heads.add(head)
                    noisy_heads_instances = noisy_heads_instances | set(inst_idices)
                else:
                    pure_clean_indices = pure_clean_indices | local_clean_set

        if len(noisy_heads) == 0:
            print(f'no noisy object (ratio > 0.3)')
            return noisy_heads, noisy_heads_instances
        uncertain_indices = list(noise_related_clean_indices)
        pure_indices = random.sample(pure_clean_indices, k=len(noise_related_clean_indices))

        return uncertain_indices, pure_indices

    def load_str_loss_entropy_data(self, sample_size, clean_threshold, uncertain_indices, pure_indices):
        file_name = self.noise_type + '_' + self.rec_model
        print(f'load detailed loss and entropy for {file_name}')
        loss_path = self.save_dir + f'/{file_name}_loss.txt'

        # get analysis len
        analysis_len = self.config['analysis_iter_length']
        with open(loss_path, encoding='utf-8') as fin:
            data_size = sum(1 for line in fin)

        clean_sample_size = min(sample_size, clean_threshold)
        clean_indices = random.sample(range(0, clean_threshold), clean_sample_size)

        noisy_sample_size = min(sample_size, data_size - clean_threshold)
        noisy_indices = random.sample(range(clean_threshold, data_size), noisy_sample_size)

        clean_losses = []
        noisy_losses = []
        clean_entropies = []
        noisy_entropies = []
        uncertain_losses = []
        pure_losses = []
        # get loss data
        with open(loss_path, encoding='utf-8') as fin:
            input_lines = fin.readlines()
            for i in clean_indices:
                values = convert_str_to_float_list(input_lines[i])[0: analysis_len]
                clean_losses.append(values)
            for i in noisy_indices:
                values = convert_str_to_float_list(input_lines[i])[0: analysis_len]
                noisy_losses.append(values)
            for i in uncertain_indices:
                values = convert_str_to_float_list(input_lines[i])[0: analysis_len]
                uncertain_losses.append(values)
            for i in pure_indices:
                values = convert_str_to_float_list(input_lines[i])[0: analysis_len]
                pure_losses.append(values)
        # get entropy data
        entropy_path = self.save_dir + f'/{file_name}_entropy.txt'
        with open(entropy_path, encoding='utf-8') as fin:
            input_lines = fin.readlines()
            for i in clean_indices:
                values = convert_str_to_float_list(input_lines[i])[0: analysis_len]
                clean_entropies.append(values)
            for i in noisy_indices:
                values = convert_str_to_float_list(input_lines[i])[0: analysis_len]
                noisy_entropies.append(values)

        self.clean_loss = np.array(clean_losses)
        self.noisy_loss = np.array(noisy_losses)
        self.clean_entropy = np.array(clean_entropies)
        self.noisy_entropy = np.array(noisy_entropies)
        self.uncertain_losses = np.array(uncertain_losses)
        self.pure_losses = np.array(pure_losses)
        self.min_loss = min(self.clean_loss.min(), self.noisy_loss.min())
        self.max_loss = max(self.clean_loss.max(), self.noisy_loss.max())
        self.min_entropy = min(self.clean_entropy.min(), self.noisy_entropy.min())
        self.max_entropy = max(self.clean_entropy.max(), self.noisy_entropy.max())
