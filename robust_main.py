import os

os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import argparse

import random
import torch
from augmentation.trainer import Trainer
from data_utils.SeqDataGenerator import SeqDataCollector
from data_utils.RankingEvaluator import print_dict
from baseline.FPMC import ProbFPMC
from baseline.Caser import ProbCaser
from baseline.GRU4Rec import RobGRU4Rec
from baseline.RobustSeqRec import RobustSeqRec
from baseline.SASRec import ProbSASRec
from baseline.RobustCaser import RobustCaser

import time

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default='ml2k')
parser.add_argument('--rec', default='caser')
args = parser.parse_args()
torch.multiprocessing.set_sharing_strategy('file_system')
# if torch.cuda.is_available():
#     torch.set_default_tensor_type('torch.cuda.FloatTensor')

def context_select(config):
    if config['rec_model'] is 'rob_gru4rec':
        return RobGRU4Rec(config)
    elif config['rec_model'] is 'rob_caser':
        return RobustCaser(config)
    elif config['rec_model'] is 'fpmc':
        return ProbFPMC(config)
    elif config['rec_model'] is 'sasrec':
        return ProbSASRec(config)
    else:
        return RobGRU4Rec(config)

if __name__ == '__main__':
    config = {
        # data settings
        'dataset': 'ml2k',
        'direction': 'comp', # bi / uni / comp
        'aug_num': 3,
        'eval_neg_num': 100,
        'max_seq_len': 200,
        'max_pred_num': 10,
        'mask_prob': 0.15,
        'dupe_num': 10,
        'train_neg_num': 20,
        'input_len': 5,
        'threshold': 0.1,  # 0.05 for steam
        'cand_num': 500,
        'noise_ratio': 0.1,
        'noise_type': 'raw',  # new, raw, no
        'item_item_window': 40,
        'thread_num': 4,
        'analysis_sample_size': 500,
        'analysis_iter_length': 200,
        # training settings
        'rec_model': 'caser',
        'train_type': 'train',  # train / analysis
        'save_loss_entropy': False,
        'epoch_num': 500,
        'learning_rate': 1e-2,
        'train_batch_size': 5000,
        'test_batch_size': 256,
        'drop_ratio': 0.1,
        # sample selection
        'paced_loss': False,
        'paced_loss_slope': False,
        'paced_entropy': False,
        'paced_entropy_slope': False,
        'forget_rate': 0.05,
        'slope_alpha': 0.05,
        'num_gradual': 20,
        # semi-regs
        'pen_entropy': False,
        'comp_num': 5,
        # prob settings
        'gamma': 1.0,
        'sample_num': 4,
        'lambda': 0.1,
        'entropy_num': 4,
        'entropy_weight': 0.01,
        # SAS settings
        'hidden_size': 32,
        'num_hidden_layers': 1,
        'num_attention_heads': 2,
        'intermediate_size': 64,
        'hidden_act': "gelu",
        'hidden_dropout_prob': 0.1,
        'attention_probs_dropout_prob': 0.1,
        'type_vocab_size': 1,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'use_item_bias': True,
        # 'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        # Caser Config
        'n_h': 16,
        'n_v': 4,
        # GRU4Rec config
        'gru_layer_num': 2,
    }

random.seed(123)


def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    config['dataset'] = args.dataset
    config['rec_model'] = args.rec
    for forget_rate in [0.08, 0.1]:
        for entropy_num in [0.5, 1, 2]:
            for comp_num in [5, 10, 15]:
                config['forget_rate'] = forget_rate
                config['entropy_num'] = entropy_num
                config['comp_num'] = comp_num
                for noise_type in ['raw']:
                    config['noise_type'] = noise_type
                    data_model = SeqDataCollector(config)
                    config['rec_model'] = 'rob_caser'
                    rec_model_1 = RobustSeqRec(config, context_select(config))
                    config['rec_model'] = 'rob_gru4rec'
                    rec_model_2 = RobustSeqRec(config, context_select(config))
                    print_dict(config, 'config')
                    trainer = Trainer(config, rec_model_1, rec_model_2, data_model, None,
                                      save_dir='./datasets/' + config['dataset'] + '/seq/')
                    trainer.run_co()
                    data_model = None
                    rec_model_1 = None
                    trainer = None








if __name__ == '__main__':
    main()



