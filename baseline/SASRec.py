import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder
import torch.nn.functional as F
from augmentation.BERT_SeqRec import BertModel
from augmentation.BERT_SeqRec import BertConfig

class SASRec(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        bert_config = BertConfig(config['item_num'], config)
        self.bert = BertModel(bert_config)
        self.user_embeddings = None

    def forward(self, user_id, hist_item_ids, masks):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        bert_context = self.bert(hist_item_ids, None)
        # [bs, hidden_size]
        context_embed = bert_context[:, -1, :].squeeze(1)

        return context_embed

class ProbSASRec(ProbContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        bert_config = BertConfig(config['item_num'], config)
        self.bert = BertModel(bert_config)
        self.item_mean_embed = self.bert.embeddings.word_embeddings
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.fc_embed = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()

    def forward(self, user_id, hist_item_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        user_mean = self.user_mean_embed(user_id)
        bert_context = self.bert(hist_item_ids, None)
        # [bs, hidden_size]
        item_context_embed = bert_context[:, -1, :].squeeze(1)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        return context_embed, torch.relu(context_var)


class RobSASRec(ProbContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        bert_config = BertConfig(config['item_num'], config)
        self.bert = BertModel(bert_config)
        self.item_mean_embed = self.bert.embeddings.word_embeddings
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.fc_embed = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()

    def forward(self, user_id, hist_item_ids, masks):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        user_mean = self.user_mean_embed(user_id)
        bert_context = self.bert(hist_item_ids, masks)
        # [bs, hidden_size]
        item_context_embed = bert_context[:, -1, :].squeeze(1)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        return context_embed, torch.relu(context_var)


from baseline.GraphSeq import RobGraphContextEncoder


class RobGraphSASRec(RobGraphContextEncoder):

    def __init__(self, config):
        super().__init__(config)
        bert_config = BertConfig(config['item_num'], config)
        self.bert = BertModel(bert_config, use_outer_embed=True)
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.fc_embed = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()

    def forward(self, user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        user_mean = user_prop_embeds[user_id]
        bert_context = self.bert(hist_item_ids, attention_mask=masks, outer_embed=item_prop_embeds)
        # [bs, hidden_size]
        item_context_embed = bert_context[:, -1, :].squeeze(1)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        # [bs, 1]
        user_var = self.user_var(user_id).unsqueeze(1)
        # [bs, seq_len, 1]
        item_vars = self.item_var(hist_item_ids)
        # [bs, seq_len + 1, 1]
        all_var = torch.cat([user_var, item_vars], dim=1)
        # [bs, 1]
        mean_var = torch.sigmoid(torch.mean(all_var, dim=1, keepdim=False))

        return context_embed, torch.relu(context_var * mean_var)


class RecentRobGraphSASRec(RobGraphSASRec):

    def __init__(self, config):
        super().__init__(config)

    def forward(self, user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        user_mean = user_prop_embeds[user_id]
        bert_context = self.bert(hist_item_ids, attention_mask=masks, outer_embed=item_prop_embeds)
        # [bs, hidden_size]
        item_context_embed = bert_context[:, -1, :].squeeze(1)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        # [bs, 1] last item
        last_item_id = hist_item_ids[:, -1]
        # [bs, hidden_size]
        last_item_embed = item_prop_embeds[last_item_id]

        return context_embed + last_item_embed, torch.relu(context_var)


class LightRobGraphSASRec(RobGraphSASRec):

    def __init__(self, config):
        super().__init__(config)

    def forward(self, user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        user_mean = user_prop_embeds[user_id]
        bert_context = self.bert(hist_item_ids, attention_mask=masks, outer_embed=item_prop_embeds)
        # [bs, hidden_size]
        item_context_embed = bert_context[:, -1, :].squeeze(1)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        return context_embed, torch.relu(context_var)





