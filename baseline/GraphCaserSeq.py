import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder
import torch.nn.functional as F


class GraphCaserSeq(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        self.n_h = config['n_h']
        self.n_v = config['n_v']
        self.drop_ratio = config['drop_ratio']

        # vertical conv layer
        self.conv_v = nn.Conv2d(1, self.n_v, (config['input_len'], 1))

        # horizontal conv layer
        lengths = [i + 1 for i in range(config['input_len'])]
        self.conv_h = nn.ModuleList([nn.Conv2d(1, self.n_h, (i, config['hidden_size'])) for i in lengths]).double()

        # fully-connected layer
        self.fc1_dim_v = self.n_v * config['hidden_size']
        self.fc1_dim_h = self.n_h * len(lengths)
        fc1_dim_in = self.fc1_dim_v + self.fc1_dim_h + config['hidden_size']
        self.fc1 = nn.Linear(fc1_dim_in, config['hidden_size']).double()

        # dropout
        self.dropout = nn.Dropout(self.drop_ratio)

    def forward(self, user_id, hist_item_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_embeddings(hist_item_ids)

        # Embedding Look-up
        item_image = hist_item_embed.unsqueeze(1)  # [bs, 1, seq_len, hidden_size]
        user_emb = self.user_embeddings(user_id)  # [bs, hidden_size]

        # Convolutional Layers
        out, out_h, out_v = None, None, None
        # vertical conv layer
        if self.n_v:
            out_v = self.conv_v(item_image)
            # [bs, n_v * hidden_size]
            out_v = out_v.view(-1, self.fc1_dim_v)  # prepare for fully connect

        # horizontal conv layer
        out_hs = list()
        if self.n_h:
            for conv in self.conv_h:
                conv_out = F.relu(conv(item_image).squeeze(3))
                # [bs, n_h]
                pool_out = F.max_pool1d(conv_out, conv_out.size(2)).squeeze(2)
                out_hs.append(pool_out)
            # [bs, n_h * input_len]
            out_h = torch.cat(out_hs, 1)  # prepare for fully connect

        # Fully-connected Layers
        out = torch.cat([out_v, out_h, user_emb], 1)
        # apply dropout
        out = self.dropout(out)

        # fully-connected layer
        context_embed = self.fc1(out)

        return context_embed


class ProbGraphCaserSeq(ProbContextEncoder):

    def __init__(self, config):
        super().__init__(config)
        self.n_h = config['n_h']
        self.n_v = config['n_v']
        self.drop_ratio = config['drop_ratio']

        # vertical conv layer
        self.conv_v = nn.Conv2d(1, self.n_v, (config['input_len'], 1))

        # horizontal conv layer
        lengths = [i + 1 for i in range(config['input_len'])]
        self.conv_h = nn.ModuleList([nn.Conv2d(1, self.n_h, (i, config['hidden_size'])) for i in lengths]).double()

        # fully-connected layer
        self.fc1_dim_v = self.n_v * config['hidden_size']
        self.fc1_dim_h = self.n_h * len(lengths)
        fc1_dim_in = self.fc1_dim_v + self.fc1_dim_h + config['hidden_size']
        self.fc1 = nn.Linear(fc1_dim_in, config['hidden_size']).double()
        self.fc_var = nn.Linear(fc1_dim_in, config['hidden_size']).double()

        # dropout
        self.dropout = nn.Dropout(self.drop_ratio)

    def forward(self, user_id, hist_item_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_mean_embed(hist_item_ids)

        # Embedding Look-up
        item_image = hist_item_embed.unsqueeze(1)  # [bs, 1, seq_len, hidden_size]
        user_emb = self.user_mean_embed(user_id)  # [bs, hidden_size]

        # Convolutional Layers
        out, out_h, out_v = None, None, None
        # vertical conv layer
        if self.n_v:
            out_v = self.conv_v(item_image)
            # [bs, n_v * hidden_size]
            out_v = out_v.view(-1, self.fc1_dim_v)  # prepare for fully connect

        # horizontal conv layer
        out_hs = list()
        if self.n_h:
            for conv in self.conv_h:
                conv_out = F.relu(conv(item_image).squeeze(3))
                # [bs, n_h]
                pool_out = F.max_pool1d(conv_out, conv_out.size(2)).squeeze(2)
                out_hs.append(pool_out)
            # [bs, n_h * input_len]
            out_h = torch.cat(out_hs, 1)  # prepare for fully connect

        # Fully-connected Layers
        out = torch.cat([out_v, out_h, user_emb], 1)
        # apply dropout
        out = self.dropout(out)

        # fully-connected layer
        context_embed = self.fc1(out)
        context_var = torch.relu(self.fc_var(out))

        return context_embed, context_var



