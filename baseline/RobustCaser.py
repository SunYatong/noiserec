import torch
from torch import nn
from augmentation.BERT_SeqRec import BertModel
from augmentation.BERT_SeqRec import BertConfig
from baseline.SeqRec import ProbContextEncoder
import torch.nn.functional as F


class RobustCaser(ProbContextEncoder):

    def __init__(self, config):
        super().__init__(config)
        self.n_h = config['n_h']
        self.n_v = config['n_v']
        self.drop_ratio = config['drop_ratio']

        # vertical conv layer
        self.conv_v = nn.Conv2d(1, self.n_v, (config['input_len'], 1))

        # horizontal conv layer
        lengths = [i + 1 for i in range(config['input_len'])]
        self.conv_h = nn.ModuleList([nn.Conv2d(1, self.n_h, (i, config['hidden_size'])) for i in lengths]).double()

        # fully-connected layer
        self.fc1_dim_v = self.n_v * config['hidden_size']
        self.fc1_dim_h = self.n_h * len(lengths)
        fc1_dim_in = self.fc1_dim_v + self.fc1_dim_h + config['hidden_size']
        self.fc1 = nn.Linear(fc1_dim_in, config['hidden_size']).double()
        self.fc_var = nn.Linear(fc1_dim_in, config['hidden_size']).double()

        # dropout
        self.dropout = nn.Dropout(self.drop_ratio)

    def forward(self, user_id, hist_item_ids, hist_comp_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        comp_ids = = [bs, seq_len, comp_num]
        """
        user_emb = self.user_mean_embed(user_id)
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_mean_embed(hist_item_ids)
        # [bs, seq_len, hidden_size]
        comped_hist_item_embed = self.comp_hist_items(user_emb, hist_item_embed, hist_comp_ids)
        # [bs, 1, seq_len, hidden_size]
        item_image = comped_hist_item_embed.unsqueeze(1)

        # Convolutional Layers
        out, out_h, out_v = None, None, None
        # vertical conv layer
        if self.n_v:
            out_v = self.conv_v(item_image)
            # [bs, n_v * hidden_size]
            out_v = out_v.view(-1, self.fc1_dim_v)  # prepare for fully connect

        # horizontal conv layer
        out_hs = list()
        if self.n_h:
            for conv in self.conv_h:
                conv_out = F.relu(conv(item_image).squeeze(3))
                # [bs, n_h]
                pool_out = F.max_pool1d(conv_out, conv_out.size(2)).squeeze(2)
                out_hs.append(pool_out)
            # [bs, n_h * input_len]
            out_h = torch.cat(out_hs, 1)  # prepare for fully connect

        # Fully-connected Layers
        out = torch.cat([out_v, out_h, user_emb], 1)
        # apply dropout
        out = self.dropout(out)

        # fully-connected layer
        context_embed = self.fc1(out)
        context_var = torch.relu(self.fc_var(out))

        return context_embed, context_var