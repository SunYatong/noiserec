import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder

class RUM(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)

    def forward(self, user_id, hist_item_ids, masks, target_id):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, hidden_size]
        user_embed = self.user_embeddings(user_id)
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_embeddings(hist_item_ids)
        # [bs, 1, hidden_size]
        target_item_embed = self.item_embeddings(target_id)
        # [bs, seq_len, 1]
        attention = torch.mul(hist_item_embed, target_item_embed).sum(dim=2, keepdim=True)
        # [bs, seq_len, hidden_size]
        weighted_item_embed = torch.mul(hist_item_embed, attention)
        # [bs, seq_len, hidden_size]
        masked_item_embed = torch.mul(weighted_item_embed, masks.unsqueeze(2))
        # [bs, hidden_size]
        sumed_item_embed = masked_item_embed.sum(dim=1, keepdim=False)
        context_embed = user_embed + sumed_item_embed

        return context_embed