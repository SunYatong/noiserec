import torch
from torch import nn
import math
import torch.nn.functional as F


class ContextEncoder(nn.Module):
    def __init__(self, config, graph=None):
        super().__init__()
        self.user_embeddings = nn.Embedding(config['user_num'], config['hidden_size'])
        self.item_embeddings = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)
        self.graph = graph


class ProbContextEncoder(nn.Module):
    def __init__(self, config, graph=None):
        super().__init__()
        self.user_mean_embed = nn.Embedding(config['user_num'] + 1, config['hidden_size'],
                                            padding_idx=config['user_num'])
        self.item_mean_embed = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)
        self.graph = graph

    def comp_hist_items(self, user_emb, hist_item_embed, hist_comp_ids):
        """
        user_emb = [bs, hidden_size]
        hist_item_embed = [bs, seq_len, hidden_size]
        comp_ids = = [bs, seq_len, comp_num]
        """
        # [bs, seq_len, comp_num, hidden_size]
        hist_item_embed_comp = self.user_mean_embed(hist_comp_ids)
        # [bs, seq_len, comp_num]
        user_comp_atten = torch.mul(user_emb.unsqueeze(1).unsqueeze(1), hist_item_embed_comp).sum(dim=3, keepdim=False)
        # [bs, seq_len, comp_num, 1]
        user_comp_atten = torch.softmax(user_comp_atten, dim=2).unsqueeze(dim=3)
        # [bs, seq_len, hidden_size]
        comp_embeds = torch.mul(user_comp_atten, hist_item_embed_comp).sum(dim=2, keepdim=False)
        comped_hist_item_embed = hist_item_embed + comp_embeds
        # [bs, seq_len, hidden_size]
        return comped_hist_item_embed


class SeqRec(nn.Module):
    def __init__(self, config, context_encoder: ContextEncoder):
        super().__init__()
        self.config = config
        self.context_encoder = context_encoder
        self.apply(self._init_parameters)

    def _init_parameters(self, module):
        """ Initialize the parameterss.
        """
        if isinstance(module, nn.Embedding):
            hidden_size = module.weight.size()[1]
            bound = 6 / math.sqrt(hidden_size)
            nn.init.uniform_(module.weight, a=-bound, b=bound)
        if isinstance(module, nn.Linear):
            torch.nn.init.xavier_normal_(module.weight, gain=1.0)
        if isinstance(module, nn.Linear) and module.bias is not None:
            module.bias.data.zero_()

    def forward(self, train_batch):
        user_id, hist_item_ids, masks, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
            # clean_mask = clean_mask.double().to(torch.device('cuda'))
            # print(f'forward, user_is_cuda: {user_id.is_cuda}')
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        neg_num = neg_targets.size()[1]
        neg_targets = neg_targets.long()
        # clean_num = clean_mask.sum()
        # noisy_num = batch_size - clean_num
        # [bs, hidden_size]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks)
        # [bs, 1]
        pos_score = torch.mul(context_embed, self.context_encoder.item_embeddings(pos_target)).sum(dim=1, keepdim=True)
        # [bs, neg_num, hidden_size]
        neg_embeds = self.context_encoder.item_embeddings(neg_targets)
        # [bs, neg_num]
        neg_scores = torch.mul(context_embed.unsqueeze(1), neg_embeds).sum(dim=2, keepdim=False)
        # [bs], BPR loss
        overall_loss = -(torch.log(torch.sigmoid(pos_score - neg_scores)) / neg_num).sum(dim=1, keepdim=False)
        # [bs]
        # overall_entropy = self.entropy(torch.cat([pos_score, neg_scores], dim=1))

        # if self.config['pen_entropy']:
        #     overall_loss = self.config['entropy_weight'] * overall_entropy + (
        #                 1 - self.config['entropy_weight']) * overall_loss

        return overall_loss, overall_loss, 0, sample_idices

    def forward_aug(self, raw_batch, aug_labels):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_target = [bs]
        """
        user_id, hist_item_ids, masks, pos_target, neg_target = raw_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.double().to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_target = neg_target.to(torch.device('cuda'))
            # print(f'forward_aug, user_is_cuda: {user_id.is_cuda}')
        # [bs], [bs, num_item], [bs, num_item], [bs]
        raw_weights, pos_augs, neg_augs, aug_weights = aug_labels
        if torch.cuda.is_available():
            raw_weights = raw_weights.to(torch.device('cuda'))
            pos_augs = pos_augs.to(torch.device('cuda'))
            neg_augs = neg_augs.to(torch.device('cuda'))
            aug_weights = aug_weights.to(torch.device('cuda'))
        # get loss on raw dataset
        # [bs, num_item], [bs, num_item], [bs]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks)
        # [bs]
        pos_score = torch.mul(context_embed, self.context_encoder.item_embeddings(pos_target)).sum(dim=1, keepdim=False)
        neg_score = torch.mul(context_embed, self.context_encoder.item_embeddings(neg_target)).sum(dim=1, keepdim=False)
        raw_loss = -torch.log(torch.sigmoid(pos_score - neg_score))
        # [1]
        raw_loss = torch.dot(raw_loss, raw_weights)
        # get loss on augmented dataset
        aug_loss = self._get_aug_loss(context_embed, pos_augs, neg_augs, aug_weights)
        return raw_loss, aug_loss

    def get_fullpred_scores(self, train_batch):
        user_id, hist_item_ids, masks, pos_target, neg_target = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.double().to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_target = neg_target.to(torch.device('cuda'))
            # print(f'get_fullpred_scores, user_is_cuda: {user_id.is_cuda}')
        # [bs, hidden_size]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks)
        # [bs, num_item] <- [bs, hidden_size] * [num_item, hidden_size]
        full_pred_scores = torch.mm(context_embed, self.context_encoder.item_embeddings.weight.transpose(0, 1))
        # [bs, 1]
        pos_score = full_pred_scores.gather(dim=1, index=pos_target.view(-1, 1))
        neg_score = full_pred_scores.gather(dim=1, index=neg_target.view(-1, 1))
        weights = torch.sigmoid(pos_score - neg_score).reshape([-1])
        # [bs, num_item], [bs]
        return full_pred_scores, weights

    def eval_ranking(self, test_batch):
        user_id, hist_item_ids, masks, target_ids = test_batch
        masks = masks.float()
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.double().to(torch.device('cuda'))
            target_ids = target_ids.to(torch.device('cuda'))
            # print(f'eval_ranking: {user_id.is_cuda}')
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, eval_neg_num + 2]
        """
        ranks = self._get_test_scores(user_id, hist_item_ids, masks, target_ids).argsort(
            dim=1, descending=True).argsort(dim=1, descending=False)[:, 0:1].float()

        # evaluate ranking
        metrics = {
            'ndcg_1': 0,
            'ndcg_5': 0,
            'ndcg_10': 0,
            'ndcg_20': 0,
            'hit_1': 0,
            'hit_5': 0,
            'hit_10': 0,
            'hit_20': 0,
            'ap': 0,
        }
        for rank in ranks:
            if rank < 1:
                metrics['ndcg_1'] += 1
                metrics['hit_1'] += 1
            if rank < 5:
                metrics['ndcg_5'] += 1 / torch.log2(rank + 2)
                metrics['hit_5'] += 1
            if rank < 10:
                metrics['ndcg_10'] += 1 / torch.log2(rank + 2)
                metrics['hit_10'] += 1
            if rank < 20:
                metrics['ndcg_20'] += 1 / torch.log2(rank + 2)
                metrics['hit_20'] += 1
            metrics['ap'] += 1.0 / (rank + 1)
        return metrics

    def _get_aug_loss(self, context_embed, pos_augs, neg_augs, aug_weights):
        """
        context_embed, [bs, hidden_size]
        pos_augs: [bs, item_num], one-hot
        neg_augs: [bs, item_num], one-hot
        aug_weights: [bs], prob dist
        """
        # [item_num, hidden_size]
        item_embeds = self.context_encoder.item_embeddings.weight
        # [bs, item_num]
        pos_embed = torch.mm(pos_augs, item_embeds)
        neg_embed = torch.mm(neg_augs, item_embeds)
        # [bs]
        pos_score = torch.mul(context_embed, pos_embed).sum(dim=1, keepdim=False)
        neg_score = torch.mul(context_embed, neg_embed).sum(dim=1, keepdim=False)
        aug_loss = -torch.log(torch.sigmoid(pos_score - neg_score))
        # [1]
        weighted_aug_loss = torch.dot(aug_loss, aug_weights)
        return weighted_aug_loss

    def _get_test_scores(self, user_id, hist_item_ids, masks, target_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, 1 + eval_neg_num]
        """
        # [bs, 1, hidden_size]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks).unsqueeze(1)
        # [bs, pred_num, hidden_size]
        target_embeds = self.context_encoder.item_embeddings(target_ids)
        # [bs, pred_num]
        scores = torch.mul(context_embed, target_embeds).sum(dim=2, keepdim=False)

        return scores


class ProbSeqRec(SeqRec):

    def __init__(self, config, prob_context_encoder: ProbContextEncoder):
        super().__init__(config, prob_context_encoder)
        self.apply(self._init_parameters)

    def forward(self, train_batch):
        user_id, hist_item_ids, pos_target, neg_targets, sampled_hist_items, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
            sampled_hist_items = sampled_hist_items.to(torch.device('cuda'))
            # clean_mask = clean_mask.double().to(torch.device('cuda'))
            # print(f'forward, user_is_cuda: {user_id.is_cuda}')
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        pos_target = pos_target.unsqueeze(1)
        # [bs, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids)

        # [bs, 1. hidden_size]
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = context_var.unsqueeze(1)
        # [bs, 1], [bs, sample_num, 1]
        pos_W_score, pos_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, pos_target)
        # [bs, neg_num], [bs, sample_num, neg_num]
        neg_W_scores, neg_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, neg_targets)
        # [bs], BPR loss
        # [bs, 1] - [bs, neg_num] = [bs, neg_num] -sum-> [bs]
        rec_W_loss = -torch.log(torch.sigmoid(pos_W_score - neg_W_scores)).mean(dim=1, keepdim=False)
        # [bs, sample_num, 1] - [bs, sample_num, neg_num]
        # = [bs, sample_num, neg_num] -mean-> [bs, sample_num] -mean-> [bs]
        rec_sample_loss = -torch.log(torch.sigmoid(pos_sample_scores - neg_sample_scores) + 1e-8).mean(dim=2, keepdim=False).mean(dim=1, keepdim=False)
        # entropy reg
        # [bs]
        var_entropy = torch.log(context_var + 1e-8).sum(dim=1)
        entropy_reg = torch.relu(self.config['gamma'] - var_entropy)
        # [bs]
        rec_loss = rec_W_loss + self.config['lambda'] * rec_sample_loss
        # return rec_loss + 0.01 * entropy_reg, rec_loss, var_entropy, rating_dist_loss, sample_idices
        return rec_loss + self.config['entropy_weight'] * entropy_reg, rec_loss, var_entropy, sample_idices

    def prediction_score(self, context_mean, context_var, target_item_ids):
        """
        context_mean = [bs, 1, hidden_size]
        context_var = [bs, 1, hidden_size]
        target_item_ids = [bs, pred_num]
        """
        # [bs, pred_num, hidden_size]
        target_means = self.context_encoder.item_mean_embed(target_item_ids)
        # target_vars = torch.relu(self.context_encoder.item_var_embed(target_item_ids))
        # [bs, pred_num]
        # mean_term = torch.square(context_mean - target_means).sum(dim=2)
        # var_term = torch.square(torch.sqrt(context_var) - torch.sqrt(target_vars)).sum(dim=2)
        # [bs, pred_num]
        W_scores = torch.mul(context_mean, target_means).sum(dim=2, keepdim=False)

        # sample scores: [bs, sample_num, 1]
        random_normal_values = torch.randn(context_var.shape[0], self.config['sample_num'], 1)
        if torch.cuda.is_available():
            random_normal_values = random_normal_values.to(torch.device('cuda')).double()
        # [bs, sample_num, hidden_size]
        context_embed = context_mean + torch.mul(context_var, random_normal_values)
        # [bs, sample_num, 1, hidden_size] * [bs, 1, pred_num, hidden_size] =
        # [bs, sample_num, pred_num, hidden_size] -sum-> [bs, sample_num, pred_num]
        sample_scores = torch.mul(context_embed.unsqueeze(2), target_means.unsqueeze(1)).sum(dim=3)
        # [bs, pred_num], [bs, sample_num, pred_num]
        return W_scores, sample_scores

    def _get_test_scores(self, user_id, hist_item_ids, masks, target_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, 1 + eval_neg_num]
        """
        # [bs, 1, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids)
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = torch.relu(context_var.unsqueeze(1))
        # [bs, pred_num], [bs, sample_num, pred_num]
        W_scores, sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, target_ids)
        return W_scores + self.config['lambda'] * sample_scores.mean(dim=1, keepdim=False)

    def entropy(self, x):
        "[bs, dim] -> [bs]"
        b = F.softmax(x, dim=1) * F.log_softmax(x, dim=1)
        b = -1.0 * b.sum(dim=1, keepdim=False)
        return b


