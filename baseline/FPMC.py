import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder


class FPMC(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)

    def forward(self, user_id, hist_item_ids, masks):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, hidden_size]
        user_embed = self.user_embeddings(user_id)
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_embeddings(hist_item_ids)
        # [bs, hidden_size]
        sumed_item_embed = hist_item_embed.sum(dim=1, keepdim=False)
        context_embed = user_embed + sumed_item_embed

        return context_embed


class ProbFPMC(ProbContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()

    def forward(self, user_id, hist_item_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, hidden_size]
        user_mean = self.user_mean_embed(user_id)
        # [bs, seq_len, hidden_size]
        hist_item_means = self.item_mean_embed(hist_item_ids)
        # [bs, hidden_size]
        sumed_item_mean = hist_item_means.sum(dim=1, keepdim=False)
        context_mean = user_mean + sumed_item_mean
        context_var = self.fc_var(torch.cat((sumed_item_mean, user_mean), dim=1))

        return context_mean, context_var



