import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder



class GRU4Rec(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        self.gru = nn.GRU(input_size=config['hidden_size'], hidden_size=config['hidden_size'],
                          num_layers=config['gru_layer_num'], dropout=config['drop_ratio'],
                          batch_first=True)
        self.user_embeddings = None

    def forward(self, user_id, hist_item_ids, masks):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_embeddings(hist_item_ids)
        output, _ = self.gru(hist_item_embed)  # (batch, seq_len, hidden_size)
        context_embed = output[:, -1, :].squeeze(1)  # (batch, hidden_size)

        return context_embed


class ProbGRU4Rec(ProbContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        self.gru = nn.GRU(input_size=config['hidden_size'], hidden_size=config['hidden_size'],
                          num_layers=config['gru_layer_num'], dropout=config['drop_ratio'],
                          batch_first=True)
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.fc_embed = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()

    def forward(self, user_id, hist_item_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, hidden_size]
        user_mean = self.user_mean_embed(user_id)
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_mean_embed(hist_item_ids)
        output, _ = self.gru(hist_item_embed)  # (batch, seq_len, hidden_size)
        item_context_embed = output[:, -1, :].squeeze(1)  # (batch, hidden_size)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_mean), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_mean), dim=1))

        return context_embed, torch.relu(context_var)


class RobGRU4Rec(ProbContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        self.gru = nn.GRU(input_size=config['hidden_size'], hidden_size=config['hidden_size'],
                          num_layers=config['gru_layer_num'], dropout=config['drop_ratio'],
                          batch_first=True)
        self.fc_var = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()
        self.fc_embed = nn.Linear(config['hidden_size'] * 2, config['hidden_size'], bias=False).double()

    def forward(self, user_id, hist_item_ids, hist_comp_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, hidden_size]
        user_emb = self.user_mean_embed(user_id)
        # [bs, seq_len, hidden_size]
        hist_item_embed = self.item_mean_embed(hist_item_ids)
        # [bs, seq_len, hidden_size]
        comped_hist_item_embed = self.comp_hist_items(user_emb, hist_item_embed, hist_comp_ids)

        output, _ = self.gru(comped_hist_item_embed)  # (batch, seq_len, hidden_size)
        item_context_embed = output[:, -1, :].squeeze(1)  # (batch, hidden_size)
        context_embed = self.fc_embed(torch.cat((item_context_embed, user_emb), dim=1))
        context_var = self.fc_var(torch.cat((item_context_embed, user_emb), dim=1))

        return context_embed, torch.relu(context_var)