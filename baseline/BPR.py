import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder


class BPR(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)

    def forward(self, user_id, hist_item_ids, masks):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, hidden_size]
        user_embed = self.user_embeddings(user_id)

        return user_embed



