import torch
from torch import nn
from baseline.SeqRec import ContextEncoder
from baseline.SeqRec import ProbContextEncoder
import torch.nn.functional as F
from augmentation.BERT_SeqRec import BertModel
from augmentation.BERT_SeqRec import BertConfig

class BERT4Rec(ContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        bert_config = BertConfig(config['item_num'], config)
        self.bert = BertModel(bert_config)
        self.user_embeddings = None

    def forward(self, user_id, hist_item_ids, masks):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = = [bs, seq_len]
        """
        # [bs, seq_len, hidden_size]
        bert_context = self.bert(hist_item_ids, None, masks)
        # [bs, hidden_size]
        context_embed = bert_context[:, -1, :].squeeze(1)

        return context_embed



