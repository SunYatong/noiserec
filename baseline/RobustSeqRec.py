import torch
from baseline.SeqRec import ProbSeqRec
from baseline.SeqRec import ProbContextEncoder
import torch.nn.functional as F
from torch import nn

class CompContextEncoder(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.user_mean_embed = nn.Embedding(config['user_num'] + 1, config['hidden_size'], padding_idx=config['user_num'])
        self.item_mean_embed = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)
        # self.user_var_embed = nn.Embedding(config['user_num'], config['hidden_size'])
        # self.item_mean_embed = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)
        # self.item_var_embed = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)
        # self.var_trans = nn.Linear(config['hidden_size'], config['hidden_size'], bias=False).double()


class RobustSeqRec(ProbSeqRec):

    def __init__(self, config, prob_context_encoder: ProbContextEncoder):
        super().__init__(config, prob_context_encoder)
        self.apply(self._init_parameters)

    def forward(self, train_batch):
        user_id, hist_item_ids, hist_comps_ids, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            hist_comps_ids = hist_comps_ids.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
            # clean_mask = clean_mask.double().to(torch.device('cuda'))
            # print(f'forward, user_is_cuda: {user_id.is_cuda}')
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        hist_comps = [bs, seq_len, comp_num]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        # [bs, 1]
        pos_target = pos_target.unsqueeze(1)
        # [bs, 1, comp_num]
        # [bs, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids, hist_comps_ids)

        # [bs, 1. hidden_size]
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = torch.relu(context_var.unsqueeze(1))
        # [bs, 1], [bs, sample_num, 1]
        pos_W_score, pos_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, pos_target)
        # [bs, neg_num], [bs, sample_num, neg_num]
        neg_W_scores, neg_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, neg_targets)
        # BPR loss
        # [bs, 1] - [bs, neg_num] = [bs, neg_num] -sum-> [bs]
        rec_W_loss = -torch.log(torch.sigmoid(pos_W_score - neg_W_scores)).mean(dim=1, keepdim=False)
        # [bs, sample_num, 1] - [bs, sample_num, neg_num]
        # = [bs, sample_num, neg_num] -mean-> [bs, sample_num] -mean-> [bs]
        rec_sample_loss = -torch.log(torch.sigmoid(pos_sample_scores - neg_sample_scores) + 1e-8).mean(dim=2, keepdim=False).mean(dim=1, keepdim=False)
        # entropy reg
        # [bs]
        var_entropy = torch.log(context_var + 1e-8).sum(dim=1)
        entropy_reg = torch.relu(self.config['gamma'] - var_entropy)
        # [bs]
        rec_loss = rec_W_loss + self.config['lambda'] * rec_sample_loss
        # return rec_loss + 0.01 * entropy_reg, rec_loss, var_entropy, rating_dist_loss, sample_idices
        return rec_loss + 0.01 * entropy_reg, rec_loss, var_entropy, sample_idices
        # return rec_W_loss, 0, sample_idices

    def _get_test_scores_comp(self, user_id, hist_item_ids, hist_comps_ids, target_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        hist_comps_ids = [bs, seq_len, comp_num]
        target_ids = [bs, 1 + eval_neg_num]
        target_comp = [bs, 1 + eval_neg_num, comp_num]
        """
        # [bs, 1, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids, hist_comps_ids)
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = torch.relu(context_var.unsqueeze(1))
        # [bs, pred_num], [bs, sample_num, pred_num]
        W_scores, sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, target_ids)
        return W_scores + self.config['lambda'] * sample_scores.mean(dim=1, keepdim=False)

    def eval_ranking(self, test_batch):
        user_id, hist_item_ids, hist_comps_ids, target_ids = test_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            hist_comps_ids = hist_comps_ids.to(torch.device('cuda'))
            target_ids = target_ids.to(torch.device('cuda'))
            # print(f'eval_ranking: {user_id.is_cuda}')
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, eval_neg_num + 2]
        """
        ranks = self._get_test_scores_comp(user_id, hist_item_ids, hist_comps_ids, target_ids).argsort(
            dim=1, descending=True).argsort(dim=1, descending=False)[:, 0:1].float()

        # evaluate ranking
        metrics = {
            'ndcg_1': 0,
            'ndcg_5': 0,
            'ndcg_10': 0,
            'ndcg_20': 0,
            'hit_1': 0,
            'hit_5': 0,
            'hit_10': 0,
            'hit_20': 0,
            'ap': 0,
        }
        for rank in ranks:
            if rank < 1:
                metrics['ndcg_1'] += 1
                metrics['hit_1'] += 1
            if rank < 5:
                metrics['ndcg_5'] += 1 / torch.log2(rank + 2)
                metrics['hit_5'] += 1
            if rank < 10:
                metrics['ndcg_10'] += 1 / torch.log2(rank + 2)
                metrics['hit_10'] += 1
            if rank < 20:
                metrics['ndcg_20'] += 1 / torch.log2(rank + 2)
                metrics['hit_20'] += 1
            metrics['ap'] += 1.0 / (rank + 1)
        return metrics
