import os

os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import argparse

import random
import torch
from augmentation.trainer import Trainer
from augmentation.trainer import Loss_Entropy_Loader
from augmentation.Augmenter import Augmenter
from data_utils.SeqDataGenerator import SeqDataCollector
from data_utils.RankingEvaluator import print_dict
from baseline.FPMC import FPMC
from baseline.Caser import Caser
from baseline.GRU4Rec import GRU4Rec
from baseline.SeqRec import SeqRec
from baseline.SASRec import SASRec
from baseline.BPR import BPR
import time

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default='ml2k')
parser.add_argument('--rec', default='fpmc')
args = parser.parse_args()
torch.multiprocessing.set_sharing_strategy('file_system')
# if torch.cuda.is_available():
#     torch.set_default_tensor_type('torch.cuda.FloatTensor')

def context_select(config):
    if config['rec_model'] == 'gru4rec':
        return GRU4Rec(config)
    elif config['rec_model'] == 'caser':
        return Caser(config)
    elif config['rec_model'] == 'fpmc':
        return FPMC(config)
    elif config['rec_model'] == 'sasrec':
        return SASRec(config)
    elif config['rec_model'] == 'bpr':
        return BPR(config)
    else:
        return GRU4Rec(config)

if __name__ == '__main__':
    config = {
        # data settings
        'dataset': 'ml1m',
        'eval_neg_num': 'full',
        'max_seq_len': 200,
        'max_pred_num': 10,
        'mask_prob': 0.15,
        'dupe_num': 10,
        'train_neg_num': 20,
        'input_len': 5,
        'threshold': 0.1,  # 0.05 for steam
        'cand_num': 'full',
        'noise_ratio': 0.1,
        'noise_type': 'raw',  # new, raw, no
        'item_item_window': 40,
        'thread_num': 4,
        'analysis_sample_size': 500,
        'analysis_iter_length': 200,
        # training settings
        'rec_model': 'caser',
        'train_type': 'train',  # train / analysis
        'save_loss_entropy': False,
        'epoch_num': 500,
        'learning_rate': 1e-2,
        'train_batch_size': 5000,
        'test_batch_size': 256,
        'drop_ratio': 0.1,
        # sample selection
        'paced_loss': True,
        'paced_loss_slope': True,
        'paced_entropy': True,
        'paced_entropy_slope': True,
        'forget_rate': 0.1,
        'slope_alpha': 0.05,
        'num_gradual': 10,
        # semi-regs
        'pen_entropy': False,
        'entropy_weight': 0.1,
        # SAS settings
        'hidden_size': 32,
        'num_hidden_layers': 1,
        'num_attention_heads': 2,
        'intermediate_size': 64,
        'hidden_act': "gelu",
        'hidden_dropout_prob': 0.1,
        'attention_probs_dropout_prob': 0.1,
        'type_vocab_size': 1,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'use_item_bias': True,
        # 'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        # Caser Config
        'n_h': 16,
        'n_v': 4,
        # GRU4Rec config
        'gru_layer_num': 2,
        'direction': 'uni,',
        'filter_test': False,
        'sample_loss_weight': 0.1,
        'save_epochs': [100],
        'weight_decay': 0.01,
        'decay_factor': 0.01,

    }

random.seed(123)


def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    config['dataset'] = 'small-changhong                              '
    config['rec_model'] = 'bpr'
    for forget_rate in [0.02, 0.04, 0.06, 0.09, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4]:
        for paced_loss in [False]:
            for paced_entropy in [False]:
                for paced_loss_slope in [False]:
                    for paced_entropy_slope in [False]:
                        config['forget_rate'] = forget_rate
                        config['paced_loss'] = paced_loss
                        config['paced_entropy'] = paced_entropy
                        config['paced_loss_slope'] = paced_loss_slope
                        config['paced_entropy_slope'] = paced_entropy_slope
                        for noise_type in ['raw']:
                            config['noise_type'] = noise_type
                            print_dict(config, 'config')
                            data_model = SeqDataCollector(config)
                            rec_model_1 = SeqRec(config, context_encoder=context_select(config))
                            # rec_model_2 = SeqRec(config, context_encoder=context_select(config))
                            trainer = Trainer(config, rec_model_1, None, data_model, None,
                                              save_dir='./datasets/' + config['dataset'] + '/seq/')
                            trainer.run_self()
                            data_model = None
                            rec_model_1 = None
                            trainer = None




if __name__ == '__main__':
    main()



