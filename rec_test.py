import os

os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import argparse

import random
import torch
from augmentation.trainer import Trainer
from data_utils.GraphDataGenerator import SeqDataCollector
from data_utils.RankingEvaluator import print_dict
from baseline.FPMC import FPMC
from baseline.GraphSeq import RobGraphSeqRec
from baseline.GRU4Rec import GRU4Rec
from baseline.Caser import Caser
from baseline.SeqRec import SeqRec
from baseline.SASRec import SASRec
from baseline.SASRec import RecentRobGraphSASRec
from baseline.SASRec import LightRobGraphSASRec
from baseline.RobustCaser import RobustCaser

import time

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default='ml1m-bert')
parser.add_argument('--rec', default='fpmc')
args = parser.parse_args()
torch.multiprocessing.set_sharing_strategy('file_system')
# if torch.cuda.is_available():
#     torch.set_default_tensor_type('torch.cuda.FloatTensor')

def context_select(config):
    if config['rec_model'] == 'rob_gru4rec':
        return None
    elif config['rec_model'] == 'fpmc':
        return FPMC(config)
    elif config['rec_model'] == 'caser':
        return Caser(config)
    elif config['rec_model'] == 'sasrec':
        return SASRec(config)
    elif config['rec_model'] == 'gru4rec':
        return GRU4Rec(config)
    elif config['rec_model'] == 'light_sasrec':
        return LightRobGraphSASRec(config)
    else:
        print("error rec model")
        return None


if __name__ == '__main__':
    config = {
        # data settings
        'dataset': 'ml2k',
        'direction': 'uni', # bi / uni / comp
        'aug_num': 3,
        'filter_test': True,
        'popularity_sample': False,
        'eval_neg_num': 100,
        'max_seq_len': 200,
        'max_pred_num': 10,
        'mask_prob': 0.15,
        'dupe_num': 10,
        'train_neg_num': 4,
        'input_len': 5,
        'threshold': 0.1,  # 0.05 for steam
        'cand_num': 500,
        'noise_ratio': 0.1,
        'noise_type': 'raw',  # new, raw, no
        'item_item_window': 40,
        'thread_num': 4,
        'analysis_sample_size': 500,
        'analysis_iter_length': 200,
        # training settings
        'rec_model': 'caser',
        'train_type': 'train',  # train / analysis
        'save_loss_entropy': False,
        'epoch_num': 500,
        'learning_rate': 1e-2,
        'train_batch_size': 5000,
        'test_batch_size': 256,
        'drop_ratio': 0.1,
        # sample selection
        'paced_loss': False,
        'paced_loss_slope': False,
        'paced_entropy': False,
        'paced_entropy_slope': False,
        'forget_rate': 0.06,
        'slope_alpha': 0.05,
        'num_gradual': 0,
        # graph settings
        'n_layers': 1,
        'next_hop_num': 1,
        'graph_dropout': False,
        # prob settings
        'entropy_threshold': 1.0,
        'sample_num': 4,
        'sample_loss_weight': 0.01,
        'entropy_num': 4,
        'entropy_reg_weight': 0.01,
        # SAS settings
        'decay_factor': 0.9,
        'hidden_size': 32,
        'num_hidden_layers': 1,
        'num_attention_heads': 2,
        'intermediate_size': 64,
        'hidden_act': "gelu",
        'hidden_dropout_prob': 0.1,
        'attention_probs_dropout_prob': 0.1,
        'type_vocab_size': 1,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'use_item_bias': True,
        # 'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
        # Caser Config
        'n_h': 16,
        'n_v': 4,
        # GRU4Rec config
        'gru_layer_num': 2,
    }

random.seed(123)


def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    for dataset in ['steam']:
        config['dataset'] = dataset
        for rec_model_name in ['fpmc', 'gru4rec', 'caser']:
            config['noise_type'] = 'raw'
            config['rec_model'] = rec_model_name
            data_model = SeqDataCollector(config)
            rec_model_1 = SeqRec(config, context_select(config))
            print_dict(config, 'config')
            trainer = Trainer(config, rec_model_1, None, data_model, None,
                              save_dir='./datasets/' + config['dataset'] + '/seq/')
            trainer.run_self()


if __name__ == '__main__':
    main()



