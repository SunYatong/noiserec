import numpy as np


# def update_mean(mean_values, indices, new_values, iter_num):
#     means_to_update = mean_values[indices]
#     mean_values[indices] = (means_to_update * iter_num + new_values) / (iter_num + 1)
#     return mean_values


def RMS_Roughness(data_list):
    """
    a numpy array of loss or entropy, e.g., [0.5, 0.3, 0.1, 0.05]
    """
    return data_list.std()


def Kurtosis(data_list):
    R_q = RMS_Roughness(data_list)
    N = len(data_list)
    Y_sum = np.power(data_list - data_list.mean(), 4).sum()
    return Y_sum / (N * np.power(R_q, 4))




