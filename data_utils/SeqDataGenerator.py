import random

random.seed(0)
import time

import multiprocessing as mp
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import numpy as np
import os
import math


class UnidirectTrainDataset(torch.utils.data.Dataset):

    def __init__(self, train_users, train_hist_items,
                 train_masks, train_targets, clean_mask, userItemSet, max_item_idx, neg_num, item_dist, aug_num=3):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        train_masks = [bs, seq_len]
        train_targets = [bs]
        clean_mask = [bs], denoting whether this instance is clean or not
        """
        assert len(train_users) == len(train_hist_items) == len(train_masks) == len(train_targets)
        self.train_users = train_users
        self.train_hist_items = train_hist_items
        self.train_masks = train_masks
        self.train_targets = train_targets
        # self.clean_mask = clean_mask
        self.train_size = len(train_users)
        self.userItemSet = userItemSet
        self.max_item_idx = max_item_idx
        self.neg_num = neg_num
        self.aug_num = aug_num
        self.input_len = len(self.train_hist_items[0])
        self.item_dist = item_dist
        self.numItem = len(item_dist)

    def __getitem__(self, index):
        userIdx = self.train_users[index]
        interacted_items = self.userItemSet[userIdx]
        final_negs = []
        while len(final_negs) < self.neg_num:
            sampled_negs = np.random.choice(self.numItem, self.neg_num, False, self.item_dist)
            valid_negs = [x for x in sampled_negs if x not in interacted_items]
            final_negs.extend(valid_negs[:])
        final_negs = final_negs[:self.neg_num]
        return userIdx, \
               torch.tensor(self.train_hist_items[index]), \
               torch.tensor(self.train_masks[index]), \
               self.train_targets[index], \
               torch.tensor(final_negs), \
               index

    def __len__(self):
        return self.train_size


class CompensateTrainDataset(torch.utils.data.Dataset):

    def __init__(self, train_users, train_hist_items, train_hist_comp,
                 train_masks, train_targets, clean_mask, userItemSet, itemUserList, max_user_idx, max_item_idx, neg_num, comp_sample_num=5):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        train_masks = [bs, seq_len]
        train_targets = [bs]
        clean_mask = [bs], denoting whether this instance is clean or not
        """
        assert len(train_users) == len(train_hist_items) == len(train_masks) == len(train_targets)
        self.train_users = train_users
        self.train_hist_items = train_hist_items
        self.train_hist_comp = train_hist_comp
        self.train_targets = train_targets
        # self.clean_mask = clean_mask
        self.train_size = len(train_users)
        self.userItemSet = userItemSet
        self.itemUserList = itemUserList
        self.max_item_idx = max_item_idx
        self.max_user_idx = max_user_idx
        self.neg_num = neg_num
        self.input_len = len(self.train_hist_items[0])
        self.comp_sample_num = comp_sample_num

    def __getitem__(self, index):
        userIdx = self.train_users[index]
        interacted_items = self.userItemSet[userIdx]
        input_items = self.train_hist_items[index]
        neg_list = []
        pos_item = self.train_targets[index]
        hist_comp_ids = self.train_hist_comp[index]

        while len(neg_list) < self.neg_num:
            sampled_neg = random.randint(1, self.max_item_idx)
            while sampled_neg in interacted_items:
                sampled_neg = random.randint(1, self.max_item_idx)
            neg_list.append(sampled_neg)
        return userIdx, \
               torch.tensor(input_items), \
               torch.tensor(hist_comp_ids), \
               pos_item, \
               torch.tensor(neg_list), \
               index

    def __len__(self):
        return self.train_size


class BidirectTrainDataset(torch.utils.data.Dataset):

    def __init__(self, train_users, train_hist_items,
                 train_masks, train_targets, clean_mask, userItemSet, max_item_idx, neg_num):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        train_masks = [bs, seq_len]
        train_targets = [bs]
        clean_mask = [bs], denoting whether this instance is clean or not
        """
        assert len(train_users) == len(train_hist_items) == len(train_masks) == len(train_targets)
        self.train_users = train_users
        self.train_hist_items = train_hist_items
        self.train_masks = train_masks
        self.train_targets = train_targets
        # self.clean_mask = clean_mask
        self.train_size = len(train_users)
        self.userItemSet = userItemSet
        self.max_item_idx = max_item_idx
        self.neg_num = neg_num
        self.seq_len = len(train_hist_items[0])

    def print_info(self, index):
        print(f'idx:{index}\t user:{self.train_users[index]}\t '
              f'hist_items:{self.train_hist_items[index]}\t target:{self.train_targets[index]}')

    def __getitem__(self, index):
        userIdx = self.train_users[index]
        interacted_items = self.userItemSet[userIdx]
        neg_set = set()
        while len(neg_set) < self.neg_num:
            sampled_neg = random.randint(1, self.max_item_idx)
            while sampled_neg in interacted_items:
                sampled_neg = random.randint(1, self.max_item_idx)
            neg_set.add(sampled_neg)

        bi_index = index + self.seq_len + 1
        rand_value = random.randint(1, 2)
        if bi_index < self.train_size and rand_value == 2 and userIdx == self.train_users[bi_index]:
            bi_hist_items = self.train_hist_items[bi_index][::-1]
            # for i in range(index, bi_index + 1):
            #     self.print_info(i)
            # if self.train_hist_items[bi_index - 1][0] is self.train_targets[index]:
            #     print('right')
            # else:
            #     print('false')
            return userIdx, \
                   torch.tensor(bi_hist_items), \
                   torch.tensor([1] * self.seq_len), \
                   self.train_targets[index], \
                   torch.tensor(list(neg_set)), \
                   index
        return userIdx, \
               torch.tensor(self.train_hist_items[index]), \
               torch.tensor(self.train_masks[index]), \
               self.train_targets[index], \
               torch.tensor(list(neg_set)), \
               index

    def __len__(self):
        return self.train_size


class UnidirectTestDataset(torch.utils.data.Dataset):

    def __init__(self, test_users, test_hist_items, test_masks, test_targets):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, pred_num]
        """
        self.test_users = test_users
        self.test_hist_items = test_hist_items
        self.test_masks = test_masks
        self.test_targets = test_targets
        self.test_size = len(test_users)

    def __getitem__(self, index):
        return self.test_users[index], \
               torch.tensor(self.test_hist_items[index]), \
               torch.tensor(self.test_masks[index]), \
               torch.tensor(self.test_targets[index]),

    def __len__(self):
        return self.test_size


class CompensateTestDataset(torch.utils.data.Dataset):

    def __init__(self, test_users, test_hist_items, test_hist_comp, test_targets, itemUserList, comp_num=5):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, pred_num]
        """
        self.test_users = test_users
        self.test_hist_items = test_hist_items
        self.test_hist_comp = test_hist_comp
        self.test_targets = test_targets
        self.test_size = len(test_users)
        self.comp_num = comp_num
        self.itemUserList = itemUserList

    def __getitem__(self, index):
        input_items = self.test_hist_items[index]
        hist_comp_ids = self.test_hist_comp[index]

        return self.test_users[index], \
               torch.tensor(input_items), \
               torch.tensor(hist_comp_ids), \
               torch.tensor(self.test_targets[index])

    def __len__(self):
        return self.test_size


def slide_window(itemList, window_size):
    """
    Input a sequence [1, 2, 3, 4, 5] with window size 3
    Return [1, 2, 3],  [2, 3, 4],  [3, 4, 5]
    with   [1, 1, 1],  [1, 1, 1],  [1, 1, 1]

    Or a sequence [1, 2, 3, 4, 5] with window size 7
    Return [0, 0, 1, 2, 3, 4, 5]
    with   [0, 0, 1, 1, 1, 1, 1]
    """
    new_item_list = [0] * (window_size - 2) + itemList
    num_seq = len(new_item_list) - window_size + 1
    assert num_seq == len(itemList) - 1
    for startIdx in range(num_seq):
        endIdx = startIdx + window_size
        item_sub_seq = new_item_list[startIdx:endIdx]
        mask = [1] * window_size
        for i, item in enumerate(item_sub_seq):
            if item == 0:
                mask[i] = 0
        yield item_sub_seq, mask


def print_single_hist_distribution(data, title, save_dir, x_label='plausibility', bins=200, x_min=0.0, x_max=1.0, color='royalblue', y_label='#instance'):
    from matplotlib import pyplot as plt
    plt.rcParams['savefig.dpi'] = 200  # 图片像素
    plt.rcParams['figure.dpi'] = 200  # 分辨率
    plt.rcParams['figure.figsize'] = (6, 4)
    from matplotlib import rcParams
    rcParams.update({'figure.autolayout': True})

    # plt.style.use('fivethirtyeight')
    fig, ax = plt.subplots()
    plt.grid(ls='-', color='silver')
    ax.hist(data, bins=bins, histtype="stepfilled", density=False, color=color)
    ax.set_xlim(x_min, x_max)
    ax.tick_params(axis='both', which='major', labelsize=19)
    # plt.show()
    plt.xlabel(x_label, fontsize=21)  # X轴标签y_
    plt.ylabel(y_label, fontsize=21)  # Y轴标签
    # x_pos = x_max * 0.7 + x_min * 0.3
    # info_str = f'TotalNum: {data.size}\n' \
    #            f'mean: {round(data.mean(), 3)}\n' \
    #            f'var: {round(data.var(), 3)}\n' \
    #            f'min: {round(data.min(), 3)}\n' \
    #            f'max: {round(data.max(), 3)}'
    # plt.text(x=x_pos, y=ax.get_ylim()[1] * 0.6, s=info_str)
    # plt.xticks(fontsize=font_size * 0.7)
    # plt.yticks(fontsize=font_size * 0.7)
    # plt.title(title)
    data_path = save_dir + f'/dist_{title}.png'
    fig.savefig(data_path)
    plt.close()


def print_double_hist_distribution(clean_data, noisy_data, title, save_dir, x_label='plausibility', bins=200, x_min=0.0, x_max=1.0, y_label='count', legend_1='user', legend_2='item', epoch=0):
    from matplotlib import pyplot as plt
    plt.rcParams['savefig.dpi'] = 200  # 图片像素
    plt.rcParams['figure.dpi'] = 200  # 分辨率
    plt.rcParams['figure.figsize'] = (6, 4)
    from matplotlib import rcParams
    rcParams.update({'figure.autolayout': True})

    fig, ax = plt.subplots()
    plt.grid(ls='-', color='silver')
    ax.hist(clean_data, bins=bins, histtype="stepfilled", density=False, color='royalblue', alpha=0.7, label=legend_1)
    ax.hist(noisy_data, bins=bins, histtype="stepfilled", density=False, color='mediumvioletred', alpha=0.7, label=legend_2)
    ax.set_xlim(0, 4)
    ax.set_ylim(0, 600) # only for ml2k user item analysis
    # plt.show()
    plt.xlabel(x_label)  # X轴标签
    plt.ylabel(y_label)  # Y轴标签
    ax.tick_params(axis='both', which='major', labelsize=19)
    # plt.show()
    plt.xlabel(x_label, fontsize=21)  # X轴标签y_
    plt.ylabel(y_label, fontsize=21)  # Y轴标签
    plt.legend(prop={'size': 19}, frameon=False)
    # x_pos = x_max * 0.73 + x_min * 0.27
    # info_str = ''
    # if clean_data.shape[0] > 0:
    #     info_str += f'#clean: {clean_data.size}\n' \
    #                 f'clean mean: {round(clean_data.mean(), 3)}\n' \
    #                 f'clean var: {round(clean_data.var(), 3)}\n' \
    #                 f'clean_min: {round(clean_data.min(), 3)}\n' \
    #                 f'clean_max: {round(clean_data.max(), 3)}\n'
    # if noisy_data.shape[0] > 0:
    #     info_str += f'#noisy: {noisy_data.size}\n' \
    #                 f'noisy mean: {round(noisy_data.mean(), 3)}\n' \
    #                 f'noisy var: {round(noisy_data.var(), 3)}\n' \
    #                 f'noisy min: {round(noisy_data.min(), 3)}\n' \
    #                 f'noisy max: {round(noisy_data.max(), 3)}'
    # plt.text(x=x_pos, y=ax.get_ylim()[1] * 0.5, s=info_str)
    plt.title(f'Epoch {epoch}',fontsize=21)
    data_path = save_dir + f'/dist_{title}.png'
    fig.savefig(data_path)
    plt.close()


class SeqDataCollector(object):

    def __init__(self, config):
        print('#' * 10 + ' DataInfo ' + '#' * 10)
        self.threshold = config['threshold']
        self.input_len = config['input_len']
        self.device = config['device']
        self.data_path = './datasets/' + config['dataset'] + '/seq/'
        self.cand_num = config['cand_num']
        self.noise_ratio = config['noise_ratio']
        self.noise_type = config['noise_type']
        self.item_item_window = config['item_item_window']
        self.train_neg_num = config['train_neg_num']
        self.thread_num = config['thread_num']
        self.direction = config['direction']
        self.filter_test = config['filter_test']
        # self.comp_num = config['comp_num']
        random.seed(123)
        np.random.seed(123)
        self.user2Idx = {}
        self.item2Idx = {'mask': 0}
        self.itemIdx2Str = {}
        self.itemIdx2Attributes = {}
        self.item_co_occur_matrix = None
        self.userIdx2sequence = {}
        self.userItemSet = {}
        self.itemUserList = {}
        self.itemUserPosition = {}
        self.attribute_freq = {}
        self.interaction_freq = {}
        self.attribute_idf_dict = {}
        self.interaction_idf_dict = {}
        self.item_content_ranked_list = {}
        self.item_interact_ranked_list = {}
        self.item_freq = {}
        self.item_idf_dict = {}
        self.content_vec_len = {}
        self.interaction_vec_len = {}
        self.valid_users = set()
        self.valid_items = set()
        self.cpu_num = min(mp.cpu_count(), 2)
        print("Number of processors: ", mp.cpu_count())

        self.numUser = 0
        self.numItem = 0
        self.item_item_content = None
        self.item_item_interaction = None
        self.item_dist = None
        self.user_dist = None
        self.load_detail_data()
        self.load_seq_data()



        config['user_num'] = self.numUser
        config['item_num'] = self.numItem
        print(f'numUser:{self.numUser}')
        print(f'numItem:{self.numItem}')
        self.max_pred_num = config['max_pred_num']
        self.eval_neg_num = config['eval_neg_num']
        self.train_batch_size = config['train_batch_size']
        self.test_batch_size = config['test_batch_size']
        self.train_size = 0
        self.valid_size = 0
        self.test_size = 0

    def load_seq_data(self):
        # load datasets
        train_count = self.load_file('train.dat')
        valid_count = self.load_file('valid.dat')
        test_count = self.load_file('test.dat')
        self.load_item_user_seq()
        self.numUser = len(self.user2Idx)
        self.numItem = len(self.item2Idx)
        # build item distribution
        self.item_dist = [0 for _ in range(self.numItem)]
        self.user_dist = [0 for _ in range(self.numUser)]
        seq_len_sum = 0
        for user, item_seq in self.userIdx2sequence.items():
            seq_len_sum += len(item_seq)
            for item in item_seq:
                self.item_dist[item] += 1
            self.user_dist[user] = len(item_seq)
        assert seq_len_sum == train_count + valid_count + test_count
        self.generate_item_dist()
        # build sim matrices
        self.item_item_content = np.zeros([self.numItem, self.numItem])
        self.item_item_interaction = np.zeros([self.numItem, self.numItem])
        self.build_content_similarity()
        self.build_interaction_similarity()
        # build noisy data filters
        self.train_filter = self.load_train_filter(f'train_filter_{self.input_len}.txt')
        self.valid_filter = self.load_valid_filter(f'valid_filter_{self.input_len}.txt')
        if self.filter_test:
            self.test_filter = self.load_valid_filter(f'test_filter_{self.input_len}.txt')
        for user, items in self.userIdx2sequence.items():
            self.userItemSet[user] = set(items)

    def load_detail_data(self):
        file_path = self.data_path + '/' + 'item_details.txt'
        if os.path.exists(file_path):
            print(f'reading item details')
            with open(file_path, encoding='utf-8') as fin:
                for line in fin:
                    itemId, itemStr, attributes = line.strip().split('\t')
                    if itemId not in self.item2Idx:
                        continue
                    attributes = set(attributes.split(','))
                    itemIdx = self.item2Idx[itemId]
                    self.itemIdx2Str[itemIdx] = itemStr
                    self.itemIdx2Attributes[itemIdx] = attributes

                    for attr in attributes:
                        if attr not in self.attribute_freq:
                            self.attribute_freq[attr] = 1
                        else:
                            self.attribute_freq[attr] += 1
        print(f'attribute num: {len(self.attribute_freq)}')

    def build_content_similarity(self):
        # load detailed attribute data
        start = time.time()
        self.load_detail_data()
        file_path = self.data_path + '/' + f'content_similarity'
        if os.path.exists(file_path + '.npy'):
            print(f'reading content_similarity')
            self.item_item_content = np.load(file_path + '.npy')
        else:
            print(f'generating content_similarity')
            # calculate idf values
            doc_num = len(self.itemIdx2Str)
            for attribute, freq in self.attribute_freq.items():
                self.attribute_idf_dict[attribute] = math.log(doc_num / (freq + 1))
            # calculate item vector lengths
            for itemIdx, attributes in self.itemIdx2Attributes.items():
                item_vec = []
                for attri in attributes:
                    attri_idf = self.attribute_idf_dict[attri]
                    item_vec.append(attri_idf)
                item_vec = np.array(item_vec)
                self.content_vec_len[itemIdx] = np.linalg.norm(item_vec)
            prepare_time = time.time()
            print(f"prepare time: {(prepare_time - start)}")
            # calculate tf-idf based cosine similarity
            for i in range(1, self.numItem):
                for j in range(i + 1, self.numItem):
                    sim_value = self.tfidf_content_similarity(i, j)[1]
                    self.item_item_content[i, j] = sim_value
                    self.item_item_content[j, i] = sim_value
            np.save(file_path, self.item_item_content)
            print(f"finish time: {(time.time() - prepare_time)}")
        for i in range(1, self.numItem):
            self.item_content_ranked_list[i] = np.sort(self.item_item_content[i])
        print(f"full finish time: {(time.time() - start)}")

    def build_interaction_similarity(self):
        begin = time.time()
        file_path = self.data_path + '/' + 'interaction_similarity'
        if os.path.exists(file_path + '.npy'):
            print(f'reading interaction_similarity')
            self.item_item_interaction = np.load(file_path + '.npy')
        else:
            print(f'generating interaction_similarity')
            # build co-occurance matrix and freq values
            doc_num = 0
            self.item_co_occur_matrix = np.zeros([self.numItem, self.numItem])
            for user, item_seq in self.userIdx2sequence.items():
                out_item_set = set(item_seq)
                in_item_set = set()
                seq_len = len(item_seq)
                step = min(int(seq_len * 0.1), int(self.item_item_window * 0.5))
                if step < 2:
                    step = 2
                seq_num = math.ceil(seq_len / step)
                for i in range(seq_num):
                    doc_num += 1
                    start = i * step
                    end = start + self.item_item_window
                    if end > seq_len:
                        end = seq_len
                    sub_seq = item_seq[start: end]
                    for item in sub_seq:
                        # freq
                        in_item_set.add(item)
                        if item not in self.interaction_freq:
                            self.interaction_freq[item] = 0
                        self.interaction_freq[item] += 1
                        # co-occurance matrix
                        for surround_item in sub_seq:
                            if item == surround_item:
                                continue
                            self.item_co_occur_matrix[item, surround_item] += 1
                # make sure the full item_seq is used
                assert len(in_item_set) == len(out_item_set)
            # get idf values
            for item, freq in self.interaction_freq.items():
                self.interaction_idf_dict[item] = math.log(doc_num / (freq + 1))
            prepare_time = time.time()
            print(f"prepare time: {(prepare_time - begin)}")
            # build_tfidf_matrix
            for i in range(1, self.numItem):
                for j in range(1, self.numItem):
                    self.item_co_occur_matrix[i, j] *= self.interaction_idf_dict[j]
                self.interaction_vec_len[i] = np.linalg.norm(self.item_co_occur_matrix[i])
            # calculate tf-idf based cosine similarity
            for i in range(1, self.numItem):
                for j in range(i + 1, self.numItem):
                    sim_value = self.tfidf_interaction_similarity(i, j)
                    self.item_item_interaction[i, j] = sim_value
                    self.item_item_interaction[j, i] = sim_value
            np.save(file_path, self.item_item_interaction)
            print(f"finish time: {(time.time() - prepare_time)}")
        for i in range(1, self.numItem):
            self.item_interact_ranked_list[i] = np.sort(self.item_item_interaction[i])
        print(f"full finish time: {(time.time() - begin)}")

    def tfidf_content_similarity(self, item_A, item_B):
        vec_A = self.itemIdx2Attributes[item_A]
        vec_B = self.itemIdx2Attributes[item_B]
        inter_set = vec_A.intersection(vec_B)
        dot_sum = 0
        for attr in inter_set:
            idf_value = self.attribute_idf_dict[attr]
            dot_sum += idf_value * idf_value
        return inter_set, dot_sum / (self.content_vec_len[item_A] * self.content_vec_len[item_B])

    def tfidf_interaction_similarity(self, item_A, item_B):
        vec_A = self.item_co_occur_matrix[item_A]
        vec_B = self.item_co_occur_matrix[item_B]
        dot_sum = np.dot(vec_A, vec_B)
        return dot_sum / (self.interaction_vec_len[item_A] * self.interaction_vec_len[item_B])

    def get_avg_sim_values(self, inputs, target):
        if target == 0:
            return 0
        content_value_sum = 0
        interaction_value_sum = 0
        valid_count = 0
        for in_id in inputs:
            if in_id == 0:
                continue
            valid_count += 1
            content_sim_value = self.item_item_content[in_id, target]
            interaction_sim_value = self.item_item_interaction[in_id, target]
            content_value_sum += content_sim_value
            interaction_value_sum += interaction_sim_value
        content_avg_sim = content_value_sum / valid_count
        interaction_avg_sim = interaction_value_sum / valid_count
        return content_avg_sim, interaction_avg_sim

    def cal_rank_ratio(self, input_ids, target, user):
        interacted_items = self.userIdx2sequence[user]
        target_content_value, target_interact_value = self.get_avg_sim_values(input_ids, target)

        # calculate content target rank ratio
        target_content_rank = np.searchsorted(self.item_content_ranked_list[target], target_content_value)
        assert target_content_rank <= self.numItem
        target_content_ratio = 1 - target_content_rank / self.numItem

        # calculate interact target rank ratio
        target_interact_rank = np.searchsorted(self.item_interact_ranked_list[target], target_interact_value)
        assert target_interact_rank <= self.numItem
        target_interact_ratio = 1 - target_interact_rank / self.numItem

        # calculate input rank ratio
        input_content_rank = 0
        input_interact_rank = 0
        valid_count = 0
        if self.cand_num == 'full':
            candidates = self.item2Idx.values()
        else:
            candidates = random.sample(set(self.item2Idx.values()), self.cand_num)
        for item in candidates:
            if item in interacted_items or item == 0:
                continue
            valid_count += 1
            content_sim, interact_sim = self.get_avg_sim_values(input_ids, target=item)
            if target_content_value < content_sim:
                input_content_rank += 1
            if target_interact_value < interact_sim:
                input_interact_rank += 1
        input_content_ratio = input_content_rank / valid_count
        input_interact_ratio = input_interact_rank / valid_count
        return input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio

    def cal_sim_rank(self, user, input_ids, pos, neg):
        interacted_items = self.userIdx2sequence[user]
        pos_sim = self.get_avg_sim_values(input_ids, pos)
        neg_sim = self.get_avg_sim_values(input_ids, neg)
        pos_rank = 0
        neg_rank = 0
        valid_count = 0
        for item in self.item2Idx.values():
            if item in interacted_items or item == 0:
                continue
            valid_count += 1
            sim_value = self.get_avg_sim_values(input_ids, target=item)
            if pos_sim < sim_value:
                pos_rank += 1
            if neg_sim < sim_value:
                neg_rank += 1
        pos_rank_ratio = pos_rank / valid_count
        neg_rank_ratio = neg_rank / valid_count
        return pos_sim, neg_sim, pos_rank_ratio, neg_rank_ratio

    def load_train_filter(self, file_name):
        file_path = self.data_path + '/new_' + file_name
        filter_dict = {}
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    userIdx, itemIdx, input_content_ratio, input_interact_ratio, \
                    target_content_ratio, target_interact_ratio = line.strip().split()
                    filter_dict[int(userIdx), int(itemIdx)] = \
                        float(input_content_ratio), float(input_interact_ratio), \
                        float(target_content_ratio), float(target_interact_ratio)
        else:
            print(f'generating filter {file_name}')
            output_lines = []
            finish_count = 0
            whole_num = len(self.userIdx2sequence.keys())
            input_len = self.input_len
            sub_seq_len = input_len + 1
            for user, item_full_seq in self.userIdx2sequence.items():
                finish_count += 1
                if finish_count % 100 == 0:
                    print(f'user {user} finished ({finish_count} / {whole_num})')
                item_train_seq = item_full_seq[0:-1]
                for sub_seq, mask in slide_window(item_train_seq, sub_seq_len):
                    input_ids = sub_seq[0: input_len]
                    target_item = sub_seq[input_len]
                    input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio = \
                        self.cal_rank_ratio(input_ids=input_ids, target=target_item, user=user)
                    filter_dict[user, target_item] = \
                        (input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio)
                    output_lines.append(f'{user}\t{target_item}\t{input_content_ratio}\t{input_interact_ratio}'
                                        f'\t{target_content_ratio}\t{target_interact_ratio}\n')
            with open(file_path, 'w', encoding='utf-8') as fout:
                fout.writelines(output_lines)
        input_content_ratios = [item[0] for item in filter_dict.values()]
        input_interact_ratios = [item[1] for item in filter_dict.values()]
        target_content_ratios = [item[2] for item in filter_dict.values()]
        target_interact_ratios = [item[3] for item in filter_dict.values()]

        print_single_hist_distribution(data=np.array(input_content_ratios),
                                       title='train_input_content_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        print_single_hist_distribution(data=np.array(input_interact_ratios),
                                       title='train_input_interact_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        print_single_hist_distribution(data=np.array(target_content_ratios),
                                       title='train_target_content_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        print_single_hist_distribution(data=np.array(target_interact_ratios),
                                       title='train_target_interact_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        return filter_dict

    def load_valid_filter(self, file_name):
        file_path = self.data_path + '/new_' + file_name
        filter_dict = {}
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    userIdx, itemIdx, input_content_ratio, input_interact_ratio, \
                    target_content_ratio, target_interact_ratio = line.strip().split()
                    filter_dict[int(userIdx), int(itemIdx)] = \
                        float(input_content_ratio), float(input_interact_ratio), \
                        float(target_content_ratio), float(target_interact_ratio)
        else:
            print(f'generating filter {file_name}')
            output_lines = []
            finish_count = 0
            whole_num = len(self.userIdx2sequence.keys())
            for user, item_full_seq in self.userIdx2sequence.items():
                finish_count += 1
                if finish_count % 100 == 0:
                    print(f'user {user} finished ({finish_count} / {whole_num})')
                if 'valid' in file_name:
                    target_item = item_full_seq[-2]
                    if len(item_full_seq) > self.input_len + 2:
                        input_ids = item_full_seq[-2 - self.input_len: -2]
                    else:
                        input_ids = item_full_seq[0: -2]
                else:
                    target_item = item_full_seq[-1]
                    if len(item_full_seq) > self.input_len + 1:
                        input_ids = item_full_seq[-1 - self.input_len: -1]
                    else:
                        input_ids = item_full_seq[0: -1]
                input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio = \
                    self.cal_rank_ratio(input_ids=input_ids, target=target_item, user=user)
                filter_dict[user, target_item] = \
                    (input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio)
                output_lines.append(f'{user}\t{target_item}\t{input_content_ratio}\t{input_interact_ratio}'
                                    f'\t{target_content_ratio}\t{target_interact_ratio}\n')
            with open(file_path, 'w', encoding='utf-8') as fout:
                fout.writelines(output_lines)
        input_content_ratios = [item[0] for item in filter_dict.values()]
        input_interact_ratios = [item[1] for item in filter_dict.values()]
        target_content_ratios = [item[2] for item in filter_dict.values()]
        target_interact_ratios = [item[3] for item in filter_dict.values()]

        print_single_hist_distribution(data=np.array(input_content_ratios),
                                       title=f'{file_name}_input_content_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        print_single_hist_distribution(data=np.array(input_interact_ratios),
                                       title=f'{file_name}_input_interact_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        print_single_hist_distribution(data=np.array(target_content_ratios),
                                       title=f'{file_name}_target_content_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        print_single_hist_distribution(data=np.array(target_interact_ratios),
                                       title=f'{file_name}_target_interact_distribution',
                                       save_dir=self.data_path + '/',
                                       x_label='rank_ratio')
        return filter_dict

    def build_instance_mappings(self, train_users, train_hist_items, train_targets):
        print('building instance mappings')
        user_instances = {}
        item_in_instances = {}
        item_out_instances = {}
        data_len = len(train_users)
        for idx in range(data_len):
            user = train_users[idx]
            out_item = train_targets[idx]
            if user not in user_instances:
                user_instances[user] = []
            if out_item not in item_out_instances:
                item_out_instances[out_item] = []
            user_instances[user].append(idx)
            item_out_instances[out_item].append(idx)

            in_items = train_hist_items[idx]
            for in_item in in_items:
                if in_item not in item_in_instances:
                    item_in_instances[in_item] = []
                item_in_instances[in_item].append(idx)

        return user_instances, item_in_instances, item_out_instances

    def generate_train_dataloader_unidirect(self):
        input_len = self.input_len
        print('generating train samples')
        start = time.time()
        train_users = []
        train_hist_items = []
        train_masks = []
        train_targets = []
        sub_seq_len = input_len + 1
        abandon_count = 0

        for user, item_full_seq in self.userIdx2sequence.items():
            item_train_seq = item_full_seq[0:-1]
            for sub_seq, mask in slide_window(item_train_seq, sub_seq_len):
                input_seq = sub_seq[0: input_len]
                input_mask = mask[0: input_len]
                target = sub_seq[input_len]
                input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio = \
                    self.train_filter[user, target]
                if input_content_ratio > self.threshold and input_interact_ratio > self.threshold \
                        and target_content_ratio > self.threshold and target_interact_ratio > self.threshold:
                    abandon_count += 1
                    continue

                assert len(sub_seq) == len(mask) == sub_seq_len
                # append lists
                train_users.append(user)
                train_hist_items.append(input_seq)
                train_masks.append(input_mask)
                train_targets.append(target)
                self.valid_users.add(user)
                self.valid_items.add(target)
                for item in sub_seq:
                    if item != 0:
                        self.valid_items.add(item)
        self.train_size = len(train_users)
        print(f"train_size: {self.train_size}, time: {(time.time() - start)}")
        print(f'abandoned {abandon_count}({round(abandon_count / self.train_size, 4)}) samples')
        print(f"valid user num: {len(self.valid_users)}")
        print(f"valid item num: {len(self.valid_items)}")

        clean_mask = None
        clean_threshold = len(train_users)
        # if self.noise_type is 'new' and self.noise_ratio > 0:
        #     train_users, train_hist_items, train_masks, train_targets, clean_mask = self.generate_new_noise_train_instances_parallel(
        #     train_users, train_hist_items, train_masks, train_targets)
        # if self.noise_type is 'raw':
        train_users, train_hist_items, train_masks, train_targets, clean_mask = self.select_existing_noise_train_instances(
            train_users, train_hist_items, train_masks, train_targets)
        if clean_mask is None:
            clean_mask = [1] * len(train_users)
        if self.direction == 'uni':
            dataset = UnidirectTrainDataset(train_users, train_hist_items,
                                        train_masks, train_targets, clean_mask, self.userItemSet,
                                        max_item_idx=self.numItem - 1, neg_num=self.train_neg_num, item_dist=self.item_dist)
        elif self.direction == 'bi':
            dataset = BidirectTrainDataset(train_users, train_hist_items,
                                            train_masks, train_targets, clean_mask, self.userItemSet,
                                            max_item_idx=self.numItem - 1, neg_num=self.train_neg_num)
        else:
            dataset = UnidirectTrainDataset(train_users, train_hist_items,
                                            train_masks, train_targets, clean_mask, self.userItemSet,
                                            max_item_idx=self.numItem - 1, neg_num=self.train_neg_num, item_dist=self.item_dist)
        dataloader = DataLoader(dataset, shuffle=True,
                                num_workers=self.cpu_num, batch_size=self.train_batch_size)
        user_instances, item_in_instances, item_out_instances = self.build_instance_mappings(train_users, train_hist_items, train_targets)
        return dataloader, clean_threshold, user_instances, item_in_instances, item_out_instances

    def build_hist_comp(self, hist_items, comp_num, max_user_idx):
        overall_hist_comp = []
        for items in hist_items:
            local_hist_comp = []
            for item in items:
                if item == 0:
                    local_hist_comp.append([max_user_idx] * comp_num)
                else:
                    local_hist_comp.append(random.sample(self.itemUserList[item], comp_num))
            overall_hist_comp.append(local_hist_comp)
        return overall_hist_comp

    def noise_sample_thread(self, in_queue, out_queue):
        while True:
            input_args = in_queue.get()
            if input_args is None:
                in_queue.task_done()
                return
            else:
                hist_items, user, sampled_idx = input_args
                n_target = np.random.choice(self.numItem, 1, False, self.item_dist)[0]
                # n_target = random.sample(self.valid_items, 1)[0]
                fail_count = 0
                failed_sampled_items = set()
                while n_target not in self.valid_items:
                    n_target = np.random.choice(self.numItem, 1, False, self.item_dist)[0]
                input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio \
                    = self.cal_rank_ratio(input_ids=hist_items, target=n_target, user=user)
                while input_content_ratio < 0.2 or input_interact_ratio < 0.2 \
                        or target_content_ratio < 0.2 or target_interact_ratio < 0.2:
                    # print(f'{sampled_idx} sampling: in_content: {input_content_ratio}, in_interact: {input_interact_ratio}, '
                    #       f' out_content: {target_content_ratio}, out_interact: {target_interact_ratio}')
                    fail_count += 1
                    failed_sampled_items.add(n_target)
                    if len(failed_sampled_items) >= self.numItem * 0.99:
                        # print(f'sample {sampled_idx} is impossible to finish')
                        break
                    # if fail_count % 1000 == 0:
                        # print(f'{sampled_idx} failed {fail_count} times')
                    n_target = np.random.choice(self.numItem, 1, False, self.item_dist)[0]
                    while n_target in failed_sampled_items:
                        n_target = np.random.choice(self.numItem, 1, False, self.item_dist)[0]
                    input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio = \
                        self.cal_rank_ratio(input_ids=hist_items, target=n_target, user=user)
                out_queue.put((sampled_idx, n_target))
                # print(f'{sampled_idx} finished')
                in_queue.task_done()

    def generate_new_noise_train_instances_parallel(self, train_users, train_hist_items, train_masks, train_targets):
        start = time.time()
        noise_targets = []
        sampled_idices = []
        noise_count = int(self.noise_ratio * self.train_size)
        clean_mask = [1] * len(train_users) + [0] * noise_count

        file_name = f'new_noise_train_samples_{self.noise_ratio}.txt'
        file_path = self.data_path + '/' + file_name
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    idx, target = line.strip().split('\t')
                    sampled_idices.append(int(idx))
                    noise_targets.append(int(target))
            noise_users = [train_users[i] for i in sampled_idices]
            noise_hist_items = [train_hist_items[i] for i in sampled_idices]
            noise_masks = [train_masks[i] for i in sampled_idices]
        else:
            output_lines = []
            print(f'generating new {noise_count} noises')
            sampled_idices = random.sample(list(range(self.train_size)), noise_count)

            input_queue = mp.JoinableQueue()
            output_queue = mp.Queue()

            for _ in range(self.thread_num):
                mp.Process(target=self.noise_sample_thread, kwargs={'in_queue': input_queue,
                                                                      'out_queue': output_queue}).start()

            for i in sampled_idices:
                input_queue.put((train_hist_items[i], train_users[i], i))

            for _ in range(self.thread_num):
                input_queue.put(None)
            input_queue.join()

            noise_users = []
            noise_hist_items = []
            noise_masks = []
            for _ in range(noise_count):
                idx, n_target = output_queue.get()
                output_lines.append(f'{idx}\t{n_target}\n')
                noise_users.append(train_users[idx])
                noise_hist_items.append(train_hist_items[idx])
                noise_masks.append(train_masks[idx])
                noise_targets.append(n_target)
            with open(file_path, 'w', encoding='utf-8') as fout:
                fout.writelines(output_lines)

        assert len(noise_users) == noise_count
        print(f'generated {len(sampled_idices)} noisy samples, time: {time.time() - start}')
        return train_users + noise_users, \
               train_hist_items + noise_hist_items, \
               train_masks + noise_masks, \
               train_targets + noise_targets,\
               clean_mask

    def select_existing_noise_train_instances(self, train_users, train_hist_items, train_masks, train_targets):
        print(f'selecting existing noises')
        clean_mask = [1] * len(train_users)
        noise_users = []
        noise_hist_items = []
        noise_masks = []
        noise_targets = []
        sub_seq_len = self.input_len + 1
        for user, item_full_seq in self.userIdx2sequence.items():
            item_train_seq = item_full_seq[0:-1]
            for sub_seq, mask in slide_window(item_train_seq, sub_seq_len):
                input_seq = sub_seq[0: self.input_len]
                input_mask = mask[0: self.input_len]
                target = sub_seq[self.input_len]
                input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio =\
                    self.train_filter[user, target]
                if input_content_ratio > self.threshold and input_interact_ratio > self.threshold \
                        and target_content_ratio > self.threshold and target_interact_ratio > self.threshold \
                        and user in self.valid_users and target in self.valid_items:
                    noise_users.append(user)
                    noise_hist_items.append(input_seq)
                    noise_masks.append(input_mask)
                    noise_targets.append(target)
        clean_mask += [0] * len(noise_users)
        print(f'selected {len(noise_users)} ({round(len(noise_users) / self.train_size, 4)}) samples')
        return train_users + noise_users, \
               train_hist_items + noise_hist_items, \
               train_masks + noise_masks, \
               train_targets + noise_targets, \
               clean_mask

    def generate_valid_dataloader_unidirect(self):
        input_len = self.input_len
        print('generating valid samples')
        start = time.time()
        valid_users = []
        valid_hist_items = []
        valid_masks = []
        valid_targets = []
        abandon_count = 0
        for user, item_full_seq in self.userIdx2sequence.items():
            target_item = item_full_seq[-2]
            input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio = \
                self.valid_filter[user, target_item]
            if input_content_ratio > self.threshold and input_interact_ratio > self.threshold \
                    and target_content_ratio > self.threshold and target_interact_ratio > self.threshold:
                abandon_count += 1
                continue
            if user not in self.valid_users or target_item not in self.valid_items:
                abandon_count += 1
                continue
            valid_users.append(user)
            raw_input = item_full_seq[0:-2]
            raw_input_len = len(raw_input)
            if raw_input_len >= input_len:
                input = raw_input[-input_len:]
                mask = [1] * input_len
            else:  # raw_input_len < input_len
                input = [0] * (input_len - raw_input_len) + raw_input
                mask = [0] * (input_len - raw_input_len) + [1] * raw_input_len
            assert len(input) == len(mask) == input_len
            assert item_full_seq[-3] == input[-1]

            valid_hist_items.append(input)
            valid_masks.append(mask)
            valid_targets.append(target_item)

        dataset = UnidirectTrainDataset(valid_users, valid_hist_items,
                                        valid_masks, valid_targets, self.userItemSet,
                                        max_item_idx=self.numItem - 1, neg_num=self.train_neg_num)
        dataloader = DataLoader(dataset, shuffle=True,
                                num_workers=self.cpu_num, batch_size=self.train_batch_size)
        self.valid_size = len(valid_users)
        print(f"valid_size: {self.valid_size}, time: {(time.time() - start)}")
        print(f'abandoned {abandon_count}({round(abandon_count / self.valid_size, 4)}) samples')
        return dataloader

    def generate_test_dataloader_unidirect(self):
        input_len = self.input_len
        print('generating test samples')
        start = time.time()
        test_users = []
        test_hist_items = []
        test_masks = []
        test_targets = []
        abandon_count = 0
        for user, item_full_seq in self.userIdx2sequence.items():
            target_item = item_full_seq[-1]
            if self.filter_test:
                input_content_ratio, input_interact_ratio, target_content_ratio, target_interact_ratio = \
                    self.test_filter[user, target_item]
                if input_content_ratio > self.threshold and input_interact_ratio > self.threshold \
                        and target_content_ratio > self.threshold and target_interact_ratio > self.threshold:
                    abandon_count += 1
                    continue
                if user not in self.valid_users or target_item not in self.valid_items:
                    abandon_count += 1
                    continue
            test_users.append(user)
            raw_input = item_full_seq[0:-1]
            raw_input_len = len(raw_input)
            if raw_input_len >= input_len:
                input = raw_input[-input_len:]
                mask = [1] * input_len
            else: # raw_input_len < input_len
                input = [0] * (input_len - raw_input_len) + raw_input
                mask = [0] * (input_len - raw_input_len) + [1] * raw_input_len
            assert len(input) == len(mask) == input_len
            assert item_full_seq[-2] == input[-1]

            test_hist_items.append(input)
            test_masks.append(mask)

            real_target = item_full_seq[-1]
            if self.eval_neg_num == 'full':
                test_targets.append([real_target] + [x for x in range(1, self.numItem) if x != real_target])
            else:
                sampled_negs = []
                while len(sampled_negs) < 101:
                    sampled_neg_cands = np.random.choice(self.numItem, self.eval_neg_num, False, self.item_dist)
                    valid_neg_ids = [x for x in sampled_neg_cands if x not in self.userItemSet[user]]
                    sampled_negs.extend(valid_neg_ids[:])
                sampled_negs = sampled_negs[:101]
                test_targets.append([item_full_seq[-1]] + list(sampled_negs))

        if self.direction == 'comp':
            test_hist_comp = self.generate_hist_comps(test_users, test_hist_items)
            dataset = CompensateTestDataset(test_users, test_hist_items, test_hist_comp, test_targets, self.itemUserList, comp_num=self.comp_num)
        else:
            dataset = UnidirectTestDataset(test_users, test_hist_items, test_masks, test_targets)
        dataloader = DataLoader(dataset, shuffle=False,
                                num_workers=self.cpu_num, batch_size=self.test_batch_size)
        self.test_size = len(test_users)
        print(f"test_size: {self.test_size}, time: {(time.time() - start)}")
        print(f'abandoned {abandon_count}({round(abandon_count / self.test_size, 4)}) samples')
        return dataloader

    def generate_item_dist(self):
        print('generating item distribution')
        item_dist = np.array(self.item_dist)
        sum_click = item_dist.sum()
        self.item_dist = item_dist / sum_click
        assert self.item_dist[0] == 0

    def load_file(self, file_name):
        file_path = self.data_path + '/' + file_name
        line_count = 0
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    splited_line = line.strip().split(' ')
                    user, item = splited_line[0], splited_line[1]
                    if user not in self.user2Idx:
                        userIdx = len(self.user2Idx)
                        self.user2Idx[user] = userIdx
                    if item not in self.item2Idx:
                        itemIdx = len(self.item2Idx)
                        self.item2Idx[item] = itemIdx
                    userIdx = self.user2Idx[user]
                    itemIdx = self.item2Idx[item]
                    if userIdx not in self.userIdx2sequence:
                        self.userIdx2sequence[userIdx] = []
                    self.userIdx2sequence[userIdx].append(itemIdx)
                    line_count += 1
        return line_count

    def load_item_user_seq(self, file_name='item_users.dat'):
        file_path = self.data_path + '/' + file_name
        if os.path.exists(file_path):
            print(f'reading {file_name}')
            with open(file_path) as fin:
                for line in fin:
                    itemId, user_seq = line.strip().split(':')
                    itemIdx = self.item2Idx[itemId]
                    userList = []
                    users = user_seq.split(',')
                    for i, user in enumerate(users):
                        userIdx = self.user2Idx[user]
                        userList.append(userIdx)
                        self.itemUserPosition[itemIdx, userIdx] = i
                    self.itemUserList[itemIdx] = userList

    def generate_hist_comps(self, users, hist_items):
        trainSize = len(users)
        hist_comps = []
        for i in range(trainSize):
            comp_i = []
            userIdx = users[i]
            items = hist_items[i]
            for itemIdx in items:
                if itemIdx == 0:
                    item_comp_i = [self.numUser] * self.comp_num
                else:
                    position = self.itemUserPosition[itemIdx, userIdx]
                    if position < self.comp_num:
                        item_comp_i = [self.numUser] * self.comp_num
                        item_comp_i[:position] = self.itemUserList[itemIdx][:position]
                    else:
                        item_comp_i = self.itemUserList[itemIdx][position - self.comp_num: position]
                comp_i.append(item_comp_i)
            hist_comps.append(comp_i)
        return hist_comps




