import random

random.seed(0)
import time

import multiprocessing as mp
import torch


def generate_train_examples_thread(user, item_seq, max_seq_len, max_pred_num, mask_prob, dupe_num):
    train_input_ids = []
    train_attention_mask = []
    train_masked_positions = []
    train_masked_ids = []
    train_label_weights = []

    # generate train input ids
    sliding_step = int(mask_prob * max_seq_len)

    if len(item_seq) <= max_seq_len:
        for _ in range(dupe_num):
            train_input_ids.append(item_seq[0:])
    else:
        offsets = list(range(0, len(item_seq) - max_seq_len, sliding_step))
        for offset in offsets:
            for _ in range(dupe_num):
                train_input_ids.append(item_seq[offset: offset + max_seq_len])
        for _ in range(dupe_num):
            train_input_ids.append(item_seq[-max_seq_len:])

    # generate masks
    for i, input_ids in enumerate(train_input_ids):
        cand_positions = list(range(0, len(input_ids)))
        random.shuffle(cand_positions)
        num_to_predict = min(max_pred_num, max(1, int(round(len(input_ids) * mask_prob))))
        masked_positions = cand_positions[0: num_to_predict]
        masked_ids = [input_ids[i] for i in masked_positions]
        train_masked_positions.append(masked_positions)
        train_masked_ids.append(masked_ids)
        # mask the input
        for position in masked_positions:
            input_ids[position] = 0
        train_input_ids[i] = input_ids

    # pad sequences
    for i in range(len(train_input_ids)):
        raw_input_len = len(train_input_ids[i])
        if raw_input_len < max_seq_len:
            seq_len_gap = max_seq_len - raw_input_len
            train_input_ids[i] += [0] * seq_len_gap
            train_attention_mask.append([1] * raw_input_len + [0] * seq_len_gap)
        else:
            train_attention_mask.append([1] * raw_input_len)
        raw_pred_num = len(train_masked_positions[i])
        if raw_pred_num < max_pred_num:
            pred_num_gap = max_pred_num - raw_pred_num
            train_masked_positions[i] += [0] * pred_num_gap
            train_masked_ids[i] += [0] * pred_num_gap
            train_label_weights.append([1] * raw_pred_num + [0] * pred_num_gap)
        else:
            train_label_weights.append([1] * raw_pred_num)

        assert len(train_input_ids[i]) == max_seq_len
        assert len(train_attention_mask[i]) == max_seq_len
        assert len(train_masked_positions[i]) == max_pred_num
        assert len(train_masked_ids[i]) == max_pred_num
        assert len(train_label_weights[i]) == max_pred_num

    return user, train_input_ids, train_attention_mask, train_masked_positions, train_masked_ids, train_label_weights


def generate_test_examples_thread(user, item_seq, max_seq_len):
    raw_len = len(item_seq)
    if raw_len < max_seq_len:
        input_ids = item_seq[0:]
    else:
        input_ids = item_seq[-max_seq_len:]
    valid_id_num = len(input_ids)
    # generate masks
    masked_positions = [len(input_ids) - 1]
    masked_ids = [input_ids[-1]]
    input_ids[-1] = 0
    # pad sequences
    if valid_id_num < max_seq_len:
        input_ids += [0] * (max_seq_len - valid_id_num)
        attention_mask = [1] * (valid_id_num - 1) + [0] * (max_seq_len - valid_id_num + 1)
    else:
        attention_mask = [1] * (valid_id_num - 1) + [0]

    assert len(input_ids) == max_seq_len
    assert len(attention_mask) == max_seq_len
    assert len(masked_positions) == 1
    assert len(masked_ids) == 1

    return user, input_ids, attention_mask, masked_positions, masked_ids


def neg_sample_thread(pos_cands, interacted_items, vocab_size, max_pred_num):
    # pos_cands: [max_pred_num]
    neg_candidates = []
    for pos in pos_cands:
        if pos is 0:
            neg_candidates.append(0)
        else:
            neg_cand = random.randint(0, vocab_size)
            while neg_cand in interacted_items:
                neg_cand = random.randint(1, vocab_size)
            neg_candidates.append(neg_cand)
    assert len(neg_candidates) == max_pred_num
    return neg_candidates


class SeqDataCollector(object):

    def __init__(self, config):
        print('#' * 10 + ' DataInfo ' + '#' * 10)
        self.device = config['device']
        self.data_path = config['data_path']
        self.user2Idx = {}
        self.item2Idx = {'mask': 0}
        self.userIdx2sequence = {}
        self.cpu_num = min(mp.cpu_count(), 12)
        print("Number of processors: ", mp.cpu_count())

        self.load_data()
        self.numUser = len(self.user2Idx)
        self.numItem = len(self.item2Idx)
        print(f'numUser:{self.numUser}')
        print(f'numItem:{self.numItem}')
        self.max_pred_num = config['max_pred_num']
        self.neg_num = config['neg_num']
        self.eval_neg_num = config['eval_neg_num']
        self.train_size = 0
        self.valid_size = 0
        self.test_size = 0

        self.user_negative_samples = {}
        self.sample_negatives_for_eval()

        self.train_collection = self.generate_train_examples(config['max_seq_len'],
                                                             config['max_pred_num'],
                                                             config['mask_prob'],
                                                             config['dupe_num'])

        self.test_batches = self.get_test_batches(
            self.generate_test_examples(config['max_seq_len']), config['valid_batch_size']
        )
        self.valid_batches = self.get_valid_batches(
            self.generate_valid_examples(config['max_seq_len']), config['valid_batch_size']
        )

    def load_data(self):
        self.load_file('train.dat')
        self.load_file('valid.dat')
        self.load_file('test.dat')

    def generate_train_examples(self, max_seq_len, max_pred_num, mask_prob, dupe_num):
        print('generating train samples')
        start = time.time()
        # Step 1: Init multiprocessing.Pool()
        pool = mp.Pool(mp.cpu_count())

        # Step 2: `pool.apply` the `howmany_within_range()`
        results = [pool.apply(generate_train_examples_thread,
                              args=(user, full_item_seq, max_seq_len, max_pred_num, mask_prob, dupe_num))
                   for user, full_item_seq in self.userIdx2sequence.items()]
        # Step 3: Don't forget to close
        pool.close()

        train_users = []
        train_input_ids = []
        train_attention_mask = []
        train_masked_positions = []
        train_masked_ids = []
        train_label_weights = []
        for examples in results:
            user, input_ids, attention_mask, masked_position, masked_ids, label_weights = examples
            train_input_ids += input_ids
            train_attention_mask += attention_mask
            train_masked_positions += masked_position
            train_masked_ids += masked_ids
            train_label_weights += label_weights
            train_users += [user] * len(input_ids)

        assert len(train_users) == len(train_input_ids) == len(train_attention_mask) \
               == len(train_masked_positions) == len(train_masked_ids) == len(train_label_weights)

        self.train_size = len(train_input_ids)
        print(f"train size: {self.train_size}, time: {(time.time() - start)}")

        return train_input_ids, \
               train_attention_mask, \
               train_masked_positions, \
               train_masked_ids, \
               train_label_weights, \
               train_users

    def get_train_batches(self, bs):
        print('generating train batches')
        start = time.time()
        train_input_ids, \
        train_attention_masks, \
        train_masked_positions, \
        train_masked_ids, \
        train_label_weights, \
        train_users = self.train_collection
        train_size = len(train_input_ids)

        train_idices = list(range(0, train_size))
        random.shuffle(train_idices)
        current_batch_pos = 0

        train_batches = []
        batch_idx = 0

        # neg_list: [neg_num, train_size, max_pred_num]
        negative_lists = self.negative_sample(masked_ids_list=train_masked_ids,
                                             user_idx_list=train_users,
                                             vocab_size=self.numItem - 1,
                                             max_pred_num=self.max_pred_num,
                                             neg_num=self.neg_num)

        while current_batch_pos < train_size:
            start_idx = current_batch_pos
            end_idx = current_batch_pos + bs
            if end_idx > train_size:
                end_idx = train_size
            current_batch_pos += bs

            batch_idx += 1
            # process_bar(batch_idx / batch_num, start_str='', end_str='&', total_length=15)

            sample_indices = train_idices[start_idx: end_idx]

            input_ids_batch = [train_input_ids[idx] for idx in sample_indices]
            attention_masks_batch = [train_attention_masks[idx] for idx in sample_indices]
            masked_positions_batch = [train_masked_positions[idx] for idx in sample_indices]
            masked_ids_batch = [train_masked_ids[idx] for idx in sample_indices]
            label_weights_batch = [train_label_weights[idx] for idx in sample_indices]
            neg_batch = []
            for neg_list in negative_lists:
                neg_batch.append(torch.tensor([neg_list[idx] for idx in sample_indices]).reshape([-1]))

            train_batches.append([torch.tensor(input_ids_batch),
                                  torch.tensor(attention_masks_batch),
                                  torch.tensor(masked_positions_batch),
                                  torch.tensor(masked_ids_batch).reshape([-1]),
                                  torch.tensor(label_weights_batch, dtype=torch.float).reshape([-1]),
                                  neg_batch])
        print(f"train_batch_num: {len(train_batches)}, time: {(time.time() - start)}")
        return train_batches

    def generate_valid_examples(self, max_seq_len):
        print(f'generating valid samples')
        start = time.time()
        # generate examples with multiprocessing
        pool = mp.Pool(mp.cpu_count())
        results = [pool.apply(generate_test_examples_thread,
                              args=(user, full_item_seq[0:-1], max_seq_len))
                   for user, full_item_seq in self.userIdx2sequence.items()]
        pool.close()
        # collect results
        valid_input_ids = []
        valid_attention_mask = []
        valid_masked_positions = []
        valid_target_ids = []
        valid_label_weights = []
        user_idices = []
        for examples in results:
            # len(input_ids) == max_seq_len
            # len(attention_mask) == max_seq_len
            # len(masked_positions) == 1
            # len(masked_ids) == 1
            user, input_ids, attention_mask, masked_position, masked_ids = examples
            valid_input_ids.append(input_ids)
            valid_attention_mask.append(attention_mask)
            valid_masked_positions.append(masked_position)
            valid_label_weights.append(1)
            valid_target_ids.append(masked_ids)
            user_idices.append(user)
        self.valid_size = len(results)
        # input_ids [bs, max_seq_len]
        # attention_mask == [bs, max_seq_len]
        # masked_positions == [bs, 1]
        # masked_ids == [bs, 1]
        # label_weights == [bs, 1]
        # user_idices == [bs]
        print(f"valid size: {self.valid_size}, time: {(time.time() - start)}")
        return valid_input_ids, \
               valid_attention_mask, \
               valid_masked_positions, \
               valid_target_ids,\
               valid_label_weights,\
               user_idices

    def get_valid_batches(self, valid_collection, bs):
        print(f'generating valid samples')
        start = time.time()
        valid_input_ids, \
        valid_attention_masks, \
        valid_masked_positions, \
        valid_target_ids, \
        valid_label_weights, \
        user_idices = valid_collection

        valid_size = len(valid_input_ids)
        valid_batches = []
        current_batch_pos = 0
        # [neg_num, list_size, 1]
        negative_lists = self.negative_sample(masked_ids_list=valid_target_ids,
                                             user_idx_list=user_idices,
                                             vocab_size=self.numItem - 1,
                                             max_pred_num=1,
                                             neg_num=self.neg_num)

        while current_batch_pos < valid_size:
            start_idx = current_batch_pos
            end_idx = start_idx + bs
            if end_idx > valid_size:
                end_idx = valid_size
            current_batch_pos += bs

            negative_list = []
            for neg_list in negative_lists:
                negative_list.append(torch.tensor(neg_list[start_idx:end_idx]).reshape([-1]))
            valid_batches.append([torch.tensor(valid_input_ids[start_idx:end_idx]),
                                  torch.tensor(valid_attention_masks[start_idx:end_idx]),
                                  torch.tensor(valid_masked_positions[start_idx:end_idx]),
                                  torch.tensor(valid_target_ids[start_idx:end_idx]).reshape([-1]),
                                  torch.tensor(valid_label_weights[start_idx:end_idx]).reshape([-1]),
                                  negative_list]),
        print(f"valid_batch_num: {len(valid_batches)}, time: {(time.time() - start)}")
        return valid_batches

    def generate_test_examples(self, max_seq_len):
        print(f'generating test samples')
        start = time.time()
        # generate examples with multiprocessing
        pool = mp.Pool(mp.cpu_count())
        results = [pool.apply(generate_test_examples_thread,
                                  args=(user, full_item_seq, max_seq_len))
                       for user, full_item_seq in self.userIdx2sequence.items()]
        pool.close()
        # collect results
        test_input_ids = []
        test_attention_mask = []
        test_masked_positions = []
        test_target_ids = []
        for examples in results:
            # len(input_ids) == max_seq_len
            # len(attention_mask) == max_seq_len
            # len(masked_positions) == 1
            # len(masked_ids) == 1
            user, input_ids, attention_mask, masked_position, masked_ids = examples
            test_input_ids.append(input_ids)
            test_attention_mask.append(attention_mask)
            test_masked_positions.append(masked_position)
            test_target_ids.append(masked_ids + self.user_negative_samples[user])
        self.test_size = len(results)

        # input_ids [bs, max_seq_len]
        # attention_mask == [bs, max_seq_len]
        # masked_positions == [bs, 1]
        # masked_ids == [bs, (1 + eval_num_num)]
        print(f"test_size: {self.test_size}, time: {(time.time() - start)}")
        return test_input_ids, \
               test_attention_mask, \
               test_masked_positions, \
               test_target_ids

    def get_test_batches(self, test_collection, bs):
        print('generating test batches')
        start = time.time()
        # input_ids [bs, max_seq_len]
        # attention_mask == [bs, max_seq_len]
        # masked_positions == [bs, 1]
        # masked_ids == [bs, (1+eval_neg_num)]
        test_input_ids, test_attention_masks, \
        test_masked_positions, test_ids = test_collection
        test_size = len(test_input_ids)

        test_batches = []
        current_batch_pos = 0
        while current_batch_pos < test_size:
            start_idx = current_batch_pos
            end_idx = start_idx + bs
            if end_idx > test_size:
                end_idx = test_size
            current_batch_pos += bs
            test_batches.append([torch.tensor(test_input_ids[start_idx:end_idx]),
                                 torch.tensor(test_attention_masks[start_idx:end_idx]),
                                 torch.tensor(test_masked_positions[start_idx:end_idx]),
                                 torch.tensor(test_ids[start_idx:end_idx])])
        print(f"test_batch_num: {len(test_batches)}, time: {(time.time() - start)}")
        return test_batches

    def load_file(self, file_name):
        file_path = self.data_path + '/' + file_name
        with open(file_path) as fin:
            for line in fin:
                user, item, rating = line.strip().split(' ')
                if user not in self.user2Idx:
                    userIdx = len(self.user2Idx)
                    self.user2Idx[user] = userIdx
                if item not in self.item2Idx:
                    itemIdx = len(self.item2Idx)
                    self.item2Idx[item] = itemIdx
                userIdx = self.user2Idx[user]
                itemIdx = self.item2Idx[item]
                if userIdx not in self.userIdx2sequence:
                    self.userIdx2sequence[userIdx] = []
                self.userIdx2sequence[userIdx].append(itemIdx)

    def sample_negatives_for_eval(self):
        print('sample negatives for eval')
        start = time.time()
        # Step 1: Init multiprocessing.Pool()
        pool = mp.Pool(mp.cpu_count())
        # Step 2: `pool.apply`
        results = [pool.apply(sample_negatives_for_eval_thread,
                              args=(user, interacted_items, self.numItem, self.eval_neg_num))
                   for user, interacted_items in self.userIdx2sequence.items()]
        # Step 3: Don't forget to close
        pool.close()

        for entry in results:
            user, negatives = entry
            self.user_negative_samples[user] = negatives
            assert len(negatives) == self.eval_neg_num
        print(f"time: {(time.time() - start)}")

    def negative_sample(self, masked_ids_list, user_idx_list, vocab_size, max_pred_num, neg_num):
        # masked_ids_list: [bs, max_pred_num]
        # user_idx_list:
        list_size = len(masked_ids_list)
        neg_list = []
        # Step 1: Init multiprocessing.Pool()
        pool = mp.Pool(mp.cpu_count())
        # Step 2: `pool.apply` the `howmany_within_range()`
        for i in range(neg_num):
            # [list_size, max_pred_num]
            results = [pool.apply(neg_sample_thread,
                                  args=(
                                      masked_ids_list[i],
                                      self.userIdx2sequence[user_idx_list[i]],
                                      vocab_size,
                                      max_pred_num))
                       for i in range(list_size)]

            # sampled_neg: [list_size * max_pred_num]
            neg_list.append(results)
        # Step 3: Don't forget to close
        pool.close()
        # neg_list: [neg_num, list_size, max_pred_num]
        return neg_list


def process_bar(percent, start_str='', end_str='', total_length=0):
    bar = ''.join(["\033[31m%s\033[0m" % '   '] * int(percent * total_length)) + ''
    bar = '\r' + start_str + bar.ljust(total_length) + ' {:0>4.1f}%|'.format(percent * 100) + end_str
    print(bar, end='', flush=True)


def sample_negatives_for_eval_thread(user, interacted_items, vocab_size, eval_neg_num):
    negatives = []
    while len(negatives) < eval_neg_num:
        neg = random.randint(1, vocab_size - 1)
        while neg in interacted_items[0:-2]:
            neg = random.randint(1, vocab_size - 1)
        negatives.append(neg)
    return user, negatives
